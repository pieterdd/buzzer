# Description
Buzzer is an application that notifies you about upcoming and ongoing events in your calendars. It is written in C# and is being developed using Visual C# 2012 with the WPF toolkit. The project was started by [Pieter De Decker](http://www.pieterdedecker.be/). Buzzer has been tested with Google Calendar, but any service that supports iCal export should - in theory at least - be compatible with Buzzer. We don't have a consumer build with a proper installer yet, but it is on the roadmap. For a list of the current limitations of Buzzer, check our [issue tracker](https://bitbucket.org/pieterdd/buzzer/issues?status=new&status=open). [This short video](https://www.youtube.com/watch?v=JcFBLO6mzbk) explains how Buzzer works.

**Warning:** This software isn't ready for day-to-day use. Buzzer's biggest limitation right now is that it ignores recurring events, so you might miss certain event reminders.

# Screenshots
![Settings window](https://bitbucket.org/pieterdd/buzzer/raw/master/Screenshots/Settings.png)

![Reminder popup](https://bitbucket.org/pieterdd/buzzer/raw/master/Screenshots/Reminder.png)

![Ongoing event notification](https://bitbucket.org/pieterdd/buzzer/raw/master/Screenshots/Ongoing.png)

# License
The GNU General Public License v3 applies to Buzzer. In addition, Buzzer builds on existing works that carry their own licenses. See [LICENSE.txt](https://bitbucket.org/pieterdd/buzzer/raw/master/LICENSE.txt) for more information.