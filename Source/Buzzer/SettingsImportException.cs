﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Model
{
    /// <summary>
    /// Raised when certain settings could not be imported.
    /// </summary>
    public class SettingsImportException : Exception
    {
        /// <summary>
        /// The instance that detected the import problem.
        /// </summary>
        private Object Plaintiff;

        public SettingsImportException(object plaintiff, String message)
            : base(message)
        {
            Plaintiff = plaintiff;
        }
    }
}
