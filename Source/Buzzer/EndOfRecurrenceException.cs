﻿using NodaTime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Model.ICSTools.Recurrence
{
    /// <summary>
    /// Thrown when something tries to calculate an occurrence that
    /// falls outside the right bound of a recurrence pattern.
    /// </summary>
    public class EndOfRecurrenceException : Exception
    {
        /// <summary>
        /// The generator that caused the exception.
        /// </summary>
        public readonly RecurrenceGenerator RecGen;

        /// <summary>
        /// The date/time that was used as a basis to calculate the occurrence
        /// that fell outside of the recurrence pattern.
        /// </summary>
        public readonly ZonedDateTime OffendingZDT;

        /// <param name="recGen">See <see cref="RecGen"/>.</param>
        /// <param name="offendingZDT">See <see cref="OffendingZDT"/>.</param>
        public EndOfRecurrenceException(RecurrenceGenerator recGen, ZonedDateTime offendingZDT)
        {
            RecGen = recGen;
            OffendingZDT = offendingZDT;
        }
    }
}
