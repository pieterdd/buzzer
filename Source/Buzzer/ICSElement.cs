﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Model.ICSTools {
    /// <summary>
    /// Used by ICSCursor to build a tree of the ICS nodes
    /// that make up a raw ICS file. All line numbers stored in
    /// this class reference to that raw ICS file.
    /// </summary>
    public class ICSElement {
        public String Type {
            get;
            private set;
        }

        /// <summary>
        /// Number of the line containing this element's BEGIN tag.
        /// </summary>
        public int BeginLine {
            get;
            private set;
        }

        /// <summary>
        /// Number of the line containing this element's END tag.
        /// </summary>
        public int EndLine {
            get;
            set;    // Because we don't know the end line when the object is created
        }

        /// <summary>
        /// List of child elements.
        /// </summary>
        public List<ICSElement> NestedElements {
            get;
            private set;
        }

        public Dictionary<String, String> Properties {
            get;
            private set;
        }

        public ICSElement(String name, int beginLine) {
            Type = name;
            BeginLine = beginLine;
            EndLine = -1;
            NestedElements = new List<ICSElement>();
            Properties = new Dictionary<string, string>();
        }
    }
}
