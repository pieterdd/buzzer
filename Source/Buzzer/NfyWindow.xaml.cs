﻿using Buzzer.Model.ICSTools;
using NodaTime;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Buzzer.Notifications
{
    /// <summary>
    /// Interaction logic for notification windows.
    /// </summary>
    public partial class NfyWindow : Window
    {
        /// <summary>
        /// The bundle of events that are to be shown to the user.
        /// </summary>
        private ICSEventBundle EvBundle;
        private ICSEventBundleEnumerator EvEnumerator;

        /// <summary>
        /// The event that is currently being shown to the user.
        /// </summary>
        private ICSEvent CurEvent
        {
            get { return EvEnumerator.Current; }
        }

        /// <summary>
        /// The storyboard that is used to coordinate the countdown animation
        /// in the progress bar.
        /// </summary>
        private Storyboard CountdownSB = new Storyboard();

        /// <summary>
        /// ID of the thread that created this instance.
        /// </summary>
        private readonly int CreatorID;

        /// <param name="ev">The calendar event that is to be shown by this dialog.</param>
        public NfyWindow(ICSEventBundle eb)
        {
            CreatorID = Environment.CurrentManagedThreadId;

            // GUI init
            InitializeComponent();
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                this.Opacity = 0;

            // Storyboard setup
            CountdownSB.Completed += NextEvent;
            Storyboard.SetTarget(CountdownSB, UICountdown);
            Storyboard.SetTargetProperty(CountdownSB, new PropertyPath(ProgressBar.ValueProperty));

            // Iterator setup
            EvBundle = eb;
            EvEnumerator = eb.GetEnumerator();

            // Start the clock when the window is loaded.
            Loaded += (sender, e) =>
            {
                NextEvent();
            };
        }

        /// <summary>
        /// Starts the progress bar animation that counts down to the next event
        /// slide.
        /// </summary>
        private void StartCountdown()
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

            // Clear the storyboard of animations
            CountdownSB.Children.Clear();

            // Construct the new animation and add it to the storyboard
            TimeSpan duration = TimeSpan.FromSeconds(GlobalConstants.NfyDisplayDuration);
            DoubleAnimation animation = new DoubleAnimation(0, duration);
            animation.From = UICountdown.Maximum;
            CountdownSB.Children.Add(animation);

            // Start the animation
            CountdownSB.Begin();
        }

        /// <summary>
        /// Selects the next event that is to be displayed and sets it as CurEvent.
        /// If all events have passed, the window is closed.
        /// </summary>
        private void NextEvent(object sender = null, EventArgs e = null)
        {
            // This needs to run on the UI thread for thread safety reasons.
            // Note that this is done synchronously to prevent the program
            // flow from being disrupted!
            Dispatcher.Invoke((Action)(() =>
            {
                Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

                // Move on and close if we reached the end of the line
                if (!EvEnumerator.MoveNext())
                {
                    CloseNotification();
                    return;
                }
                
                // UI updates, restart countdown
                UpdateView();
                StartCountdown();
            }));
        }

        /// <summary>
        /// Makes sure that the current event is reflected in the UI.
        /// </summary>
        private void UpdateView()
        {
            Instant now = SystemClock.Instance.Now;

            LblNTitle.Content = ComposeWindowTitle();
            LblEName.Content = CurEvent.Summary;
            LblESub.Content = ComposeSubtitle();
            EvProg.CurEvent = CurEvent;

            // Show an alert bell for upcoming events
            BellImg.Visibility = System.Windows.Visibility.Collapsed;
            EvProg.Visibility = System.Windows.Visibility.Collapsed;
            if (now < CurEvent.Start)
                BellImg.Visibility = System.Windows.Visibility.Visible;
            else
                EvProg.Visibility = System.Windows.Visibility.Visible;
        }

        /// <summary>
        /// Closes the window on the GUI thread in response to the 'X'
        /// or the notification timer.
        /// </summary>
        private void CloseNotification(object sender = null, EventArgs e = null)
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

            // Do a fadeout animation
            var anim = new DoubleAnimation(0, TimeSpan.FromMilliseconds(GlobalConstants.NfyFadeOutDuration));
            anim.Completed += (s, _) => Close();
            BeginAnimation(UIElement.OpacityProperty, anim);
        }

        /// <summary>
        /// Composes the string that is placed under the name of the current event.
        /// </summary>
        /// <returns></returns>
        private String ComposeSubtitle()
        {
            Instant now = SystemClock.Instance.Now;
            if (now < CurEvent.Start)
                return "Starts " + CurEvent.ComposeRelativeStart().ToLower();
            else if (now < CurEvent.End)
                return "Ends " + CurEvent.ComposeRelativeEnd().ToLower();
            else
                return "Ended " + CurEvent.ComposeRelativeEnd().ToLower();
        }

        /// <summary>
        /// Composes the window title based on the current event.
        /// </summary>
        private String ComposeWindowTitle()
        {
            Instant now = SystemClock.Instance.Now;

            // Regular events
            if (!CurEvent.Daywide)
            {
                if (now < CurEvent.Start)
                    return "Event Reminder";
                else if (CurEvent.Start <= now && now <= CurEvent.End)
                    return "Ongoing Event";
                else
                    return "Past Event";
            }
            // Daywide events
            else
            {
                ZonedDateTime zdtNow = now.InZone(DateTimeZoneProviders.Tzdb.GetSystemDefault());
                if (zdtNow.Date < CurEvent.StartDate)
                    return "Event Reminder";
                else if (CurEvent.StartDate <= zdtNow.Date && zdtNow.Date <= CurEvent.EndDate)
                    return "Ongoing Event";
                else
                    return "Past Event";
            }
        }

        /// <summary>
        /// Executed when a mouse enters the window.
        /// </summary>
        private void ProcessMouseEnter(object sender, MouseEventArgs e)
        {
            // Pause the countdown animation
            CountdownSB.Pause();
        }

        /// <summary>
        /// Executed when a mouse leaves the window.
        /// </summary>
        private void ProcessMouseLeave(object sender, MouseEventArgs e)
        {
            // Resume the countdown animation
            CountdownSB.Resume();
        }
    }
}
