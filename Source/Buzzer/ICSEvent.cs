﻿using Buzzer.Model.ICSTools.Exceptions;
using NodaTime;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Model.ICSTools {
    /// <summary>
    /// Represents a single occurrence of an event from an ICS calendar. Thread-safe.
    /// </summary>
    public class ICSEvent {
        /// <summary>Informal event name.</summary>
        public readonly String Summary;

        /// <summary>Start of the event. Note that the meaning of this field is slightly
        /// different for Daywide events in terms of how they are to be interpreted.</summary>
        public readonly Instant Start;

        /// <summary>Analoguous to <seealso cref="ICSEvent.Start"/>.</summary>
        public readonly Instant End;

        /// <summary>
        /// If true, the time components of Start/End are to be disregarded. In
        /// addition, daywide events don't have a timezone. To see if a daywide event
        /// is ongoing, we compare the system clock's date in the local time zone
        /// to Start/End's date in UTC.
        /// </summary>
        public readonly bool Daywide;

        /// <summary>
        /// The day on which this event starts/ends. Only valid for daywide events; refer
        /// to <seealso cref="ICSEvent.Daywide"/>.
        /// </summary>
        public LocalDate StartDate
        {
            get
            {
                if (!Daywide)
                    throw new ICSInterpretationException("Not a daywide event", this);
                return Start.InUtc().LocalDateTime.Date;
            }
            private set { }
        }
        /// <summary>Analogous to <see cref="ICSEvent.StartDate"/>.</summary>
        public LocalDate EndDate
        {
            get
            {
                if (!Daywide)
                    throw new ICSInterpretationException("Not a daywide event", this);

                // Compensate for ICS behavior that doesn't make sense in this context.
                // We have to subtract one day from the end timestamp to obtain the last
                // day that the event will be taking place. At the same time, we also
                // want to make sure that end dates never fall before the start date.
                LocalDate startDate = StartDate;
                LocalDate endDate = End.InUtc().LocalDateTime.Date;
                Period diff = Period.Between(startDate, endDate, PeriodUnits.Days);
                if (diff.Days <= 1)
                    return startDate;
                else
                    return endDate.PlusDays(-1);
            }
            private set { }
        }

        /// <summary>
        /// List of times when we should display a reminder for this event.
        /// </summary>
        public List<Instant> Alarms
        {
            get;
            private set;
        }

        public ICSEvent(String summary, Instant start, Instant end, bool dayWide, List<Instant> alarms) {
            Summary = summary;
            Start = start;
            End = end;
            Daywide = dayWide;
            Alarms = alarms;

            // Don't allow alarms for daywide events. We haven't decided if/how we
            // want to accept them.
            Debug.Assert(!Daywide || Alarms.Count == 0);
        }

        /// <summary>
        /// Composes a relative description of the event's starting time.
        /// </summary>
        public String ComposeRelativeStart()
        {
            // Regular events
            if (!Daywide)
                return ComposeRelativeTimestamp_Regular(Start);
            // Daywide events
            else
                return ComposeRelativeTimestamp_Daywide(StartDate);
        }

        /// <summary>
        /// Composes a relative description of the event's ending time.
        /// </summary>
        public String ComposeRelativeEnd()
        {
            // Regular events
            if (!Daywide)
                return ComposeRelativeTimestamp_Regular(End);
            // Daywide events
            else
                return ComposeRelativeTimestamp_Daywide(EndDate);
        }

        /// <summary>
        /// Composes a relative timestamp for the start or end time. Used
        /// exclusively for regular events.
        /// </summary>
        private String ComposeRelativeTimestamp_Regular(Instant refTime)
        {
            Debug.Assert(!Daywide);
            Instant now = SystemClock.Instance.Now;
            ZonedDateTime zdtNow = now.InZone(DateTimeZoneProviders.Tzdb.GetSystemDefault());
            ZonedDateTime zdtRef = refTime.InZone(DateTimeZoneProviders.Tzdb.GetSystemDefault());

            // Get difference in seconds
            NodaTime.Duration diff = now.Minus(refTime);
            long absTickDiff = (long)Math.Floor(Math.Abs(diff.Ticks / Math.Pow(10, 7)));

            return ConvertTicksToMeasurement(now, refTime, zdtNow, zdtRef, absTickDiff);
        }
        
        /// <summary>
        /// Converts the positive difference in ticks with a reference time into something like
        /// '2 seconds', '6 minutes', etc.
        /// </summary>
        private String ConvertTicksToMeasurement(Instant now, Instant refTime, ZonedDateTime zdtNow, ZonedDateTime zdtRef, long absTickDiff)
        {
            double measurement = 0;
            String unit = "";

            if (absTickDiff == 0)
                return "Just now";
            // Seconds notation
            else if (absTickDiff < 60)
            {
                measurement = absTickDiff;
                unit = "second";
            }
            // Minutes notation
            else if (absTickDiff < 60 * 60)
            {
                measurement = absTickDiff / 60.0;
                unit = "minute";
            }
            // Hours notation
            else if (absTickDiff < 60 * 60 * 24)
            {
                measurement = absTickDiff / 60.0 / 60.0;
                unit = "hour";
            }
            // Days notation (max. 30 days difference)
            else if (absTickDiff <= 60 * 60 * 24 * 30)
            {
                measurement = absTickDiff / 24.0 / 60.0 / 60.0;
                unit = "day";
            }
            // At least within the current calendar year?
            else if (zdtNow.Year == zdtRef.Year)
            {
                DateTimeOffset dtRef = zdtRef.ToDateTimeOffset();
                return dtRef.ToString("MMM d");
            }
            // Screw it... we're going with a generic timestamp.
            else
            {
                DateTimeOffset dtRef = zdtRef.ToDateTimeOffset();
                return dtRef.ToString("MMM d, yyyy");
            }

            // Pluralize measurement if needed
            if (measurement != 1)
                unit += "s";

            // Last decision: past or present?
            if (now < refTime)
                return "In " + Math.Ceiling(measurement) + " " + unit;
            else
                return Math.Floor(measurement) + " " + unit + " ago";
        }

        /// <summary>
        /// Composes a relative timestamp for the start or end time. Used
        /// exclusively for daywide events.
        /// </summary>
        private String ComposeRelativeTimestamp_Daywide(LocalDate refDate)
        {
            Debug.Assert(Daywide);
            Instant now = SystemClock.Instance.Now;
            ZonedDateTime zdtNow = now.InZone(DateTimeZoneProviders.Tzdb.GetSystemDefault());
            LocalDate nowDate = zdtNow.Date;
            Period difference = Period.Between(refDate, nowDate, PeriodUnits.Days);
            long absDayDiff = Math.Abs(difference.Days);

            // Well that was a freebie.
            if (nowDate.Equals(refDate))
                return "Today";
            // Not in this year?
            else if (nowDate.Year != refDate.Year)
            {
                DateTime dt = new DateTime(refDate.Year, refDate.Month, refDate.Day);
                return dt.ToString("MMM d, yyyy");
            }
            // The yesterday/tomorrow filter
            else if (absDayDiff == 1)
            {
                if (refDate < nowDate)
                    return "Yesterday";
                else
                    return "Tomorrow";
            }
            // Within the same week
            else if (absDayDiff <= 7)
            {
                if (refDate < nowDate)
                    return absDayDiff + " days ago";
                else
                    return "In " + absDayDiff + " days";
            }
            // Generic case for dates within the same year
            else
            {
                DateTime dt = new DateTime(refDate.Year, refDate.Month, refDate.Day);
                return dt.ToString("d");
            }
        }

        /// <summary>
        /// Generates textual representation of an ICSEvent object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            String result = Summary + " (";
            if (!Daywide)
                result += Start.ToString();
            else
                result += StartDate.ToString();
            result += ")";
            return result;
        }

        /// <summary>Decides whether two ICSEvent are valuewise equals.</summary>
        public override bool Equals(Object theirs)
        {
            if ((theirs == null) || !this.GetType().Equals(theirs.GetType()))
                return false;
            ICSEvent theirEvent = (ICSEvent)theirs;
            if (!Summary.Equals(theirEvent.Summary))
                return false;
            if (!Start.Equals(theirEvent.Start))
                return false;
            if (!End.Equals(theirEvent.End))
                return false;
            if (Daywide != theirEvent.Daywide)
                return false;
            if (Alarms.Count != theirEvent.Alarms.Count)
                return false;
            for (int i = 0; i < Alarms.Count; ++i)
                if (!Alarms[i].Equals(theirEvent.Alarms[i]))
                    return false;

            return true;
        }

        /// <summary>
        /// Creates a hash code for this ICSEvent using a trick described by
        /// Jon Skeet on http://stackoverflow.com/a/263416/217649.
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + Summary.GetHashCode();
            hash = hash * 23 + Start.GetHashCode();
            hash = hash * 23 + End.GetHashCode();
            hash = hash * 23 + Daywide.GetHashCode();
            foreach (Instant alarm in Alarms)
                hash = hash * 23 + alarm.GetHashCode();

            return hash;
        }
    }
}
