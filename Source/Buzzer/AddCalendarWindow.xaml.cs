﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Buzzer.View
{
    /// <summary>
    /// Interaction logic for AddCalendarWindow.xaml
    /// </summary>
    public partial class AddCalendarWindow : Window
    {
        /// <summary>The URL that the user entered.</summary>
        public String UserURL
        {
            get { return UITxtURL.Text; }
            private set { }
        }

        public AddCalendarWindow()
        {
            InitializeComponent();
            UITxtURL.SelectAll();
        }

        private void ClickOK(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void ClickCancel(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
