﻿using Buzzer.Model.ICSTools;
using NodaTime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace Buzzer.Model {
    /// <summary>
    /// Represents an ICS-formatted calendar source and is responsible for generating
    /// alerts originating from this calendar. Public fields/properties of this class
    /// are thread-safe.
    /// </summary>
    public class Calendar : IXmlSerializable, INotifyPropertyChanged {
        /// <summary>
        /// The URL of this calendar.
        /// </summary>
        public String DataUrl
        {
            // Atomic by default: http://bit.ly/csharpatomicity
            get;
            private set;
        }

        /// <summary>Keeps tabs on the events in this calendar.</summary>
        private ICSCache Cache = new ICSCache();

        // Background workers that assist in the downloading of calendar updates
        // and the updating of the cache.
        private BackgroundWorker Fetcher = new BackgroundWorker();
        private BackgroundWorker CacheUpdater = new BackgroundWorker();
        
        /// <summary>Indicates whether we have been able to reach this calendar recently. Made
        /// thread-safe internally.</summary>
        public bool Online {
            // Atomic by default: http://bit.ly/csharpatomicity
            get {
                lock (OnlineLock)
                {
                    return INTERNAL_Online;
                }
            }
            private set
            {
                bool changed = false;
                lock (OnlineLock) {
                    if (INTERNAL_Online != value)
                    {
                        changed = true;
                        INTERNAL_Online = value;
                    }   
                }

                // With the lock released, we can issue a PropertyChanged notification
                // to observers on the creating thread.
                if (changed)
                    CreatorDispatcher.Invoke((Action)(() =>
                    {
                        NotifyPropertyChanged();
                        NotifyPropertyChanged("Status");
                    }));
            }
        }
        private bool INTERNAL_Online = false; // Don't touch directly, use Online instead!
        private Object OnlineLock = new Object(); // Thread sync lock for Online

        /// <summary>Indicates whether a calendar download is currently in progress. Caution: since
        /// this is a multithreaded program, there's no guarantee that this won't change from one moment
        /// to the next!</summary>
        public bool IsDownloading
        {
            get {
                Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
                return Fetcher.IsBusy;
            }
        }

        /// <summary>
        /// Informal calendar name.
        /// </summary>
        public String Name {
            get { return Cache.Name; }
        }

        /// <summary>Provides access to the last list of newly active events/reminders.</summary>
        public ICSEventBundle NewlyActiveEvents
        {
            get { return Cache.NewlyActiveEvents; }
        }

        /// <summary>The moment when the last calendar redownload was completed. Will contain an
        /// instant with 0 ticks if no update has taken place so far.</summary>
        private Instant LastDownloadTime
        {
            get { return INTERNAL_LastDownloadTime; }
            set {
                // Propagate the change and send property change notifications for all affected
                // fields, either internal or external.
                INTERNAL_LastDownloadTime = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("LastDownload");
            }
        }
        private Instant INTERNAL_LastDownloadTime = Instant.FromTicksSinceUnixEpoch(0); // Don't change this directly!

        /// <summary>String representation of the last download time.</summary>
        public String LastDownload
        {
            get {
                return GetInformalInstant(LastDownloadTime);
            }
        }

        /// <summary>Time of the last calendar change that was detected.</summary>
        private Instant LastChangeTime
        {
            get { return INTERNAL_LastChangeTime; }
            set
            {
                // Propagate the change and send property change notifications for all affected
                // fields, either internal or external.
                INTERNAL_LastChangeTime = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("LastChange");
            }
        }
        private Instant INTERNAL_LastChangeTime = Instant.FromTicksSinceUnixEpoch(0); // Don't change this directly!

        /// <summary>String representing the time of the last calendar change that was detected.</summary>
        public String LastChange
        {
            get
            {
                return GetInformalInstant(LastChangeTime);
            }
        }

        /// <summary>Gets/sets whether default reminders are enabled for events without alarms.</summary>
        public bool DefaultReminderEnabled
        {
            get
            {
                Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
                return Cache.DefaultReminderEnabled;
            }
            set
            {
                Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
                Cache.DefaultReminderEnabled = value;
            }
        }

        /// <summary>Gets/sets the default reminder offset for events without alarms.</summary>
        public long DefaultReminderOffset
        {
            get {
                Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
                return Cache.DefaultReminderOffset;
            }
            set {
                Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
                Cache.DefaultReminderOffset = value;
            }
        }

        // == START OF HANDLERS AND DELEGATES
        /// <summary>Raised when certain calendar properties change to trigger
        /// updates of UI elements.</summary>
        public event PropertyChangedEventHandler PropertyChanged;

        public delegate void FetchedHandler(object sender, EventArgs args);
        public delegate void CacheUpdateHandler(object sender, EventArgs args);
        public delegate void FetchFailedHandler(Calendar sender, ErrorEventArgs args);

        /// <summary>Raises when a calendar download has succeeded.</summary>
        public event FetchedHandler Fetched;

        /// <summary>Raised when the cache has been updated.</summary>
        public event CacheUpdateHandler CacheUpdated;
        
        /// <summary>Raised when a calendar download failed for any reason.</summary>
        public event FetchFailedHandler FetchFailed;
        // == END OF HANDLERS AND DELEGATES

        /// <summary>
        /// Human-readable status of this calendar.
        /// </summary>
        public String Status
        {
            get
            {
                Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
                if (Fetcher.IsBusy)
                    return "Updating...";
                else if (Online)
                    return "Online";
                else
                    return "Offline";
            }
        }

        /// <summary>For debugging purposes. Lets us check if certain things are running on
        /// the right thread.</summary>
        private readonly int CreatorID;

        /// <summary>Lets us schedule code on the creator thread.</summary>
        private readonly Dispatcher CreatorDispatcher;

        /// <summary>Schedules an automatic calendar redownload every so often.</summary>
        private Timer AutoFetchTimer = new Timer();

        /// <summary>
        /// Constructor for normal use.
        /// </summary>
        public Calendar(String dataUrl) {
            CreatorID = Environment.CurrentManagedThreadId;
            CreatorDispatcher = Dispatcher.CurrentDispatcher;
            DataUrl = dataUrl;
            Construct();
        }

        /// <summary>
        /// Incomplete constructor that is to be used in conjunction
        /// with serialization only.
        /// </summary>
        private Calendar() {
            CreatorID = Environment.CurrentManagedThreadId;
            CreatorDispatcher = Dispatcher.CurrentDispatcher;
            Construct();
        }

        /// <summary>
        /// Shared constructor tasks.
        /// </summary>
        private void Construct() {
            // BackgroundWorkers that are created on non-GUI threads have annoying
            // side effects. Therefore we should always create them on the GUI thread.
            Debug.Assert(CreatorID == (int)Application.Current.Properties["LastGUIThreadID"]);
            
            Fetcher.DoWork += new DoWorkEventHandler(FetchBackground);
            Fetcher.RunWorkerCompleted += new RunWorkerCompletedEventHandler(FetchCompleted);
            CacheUpdater.DoWork += new DoWorkEventHandler(UpdateCacheBackground);
            CacheUpdater.RunWorkerCompleted += new RunWorkerCompletedEventHandler(UpdateCacheCompleted);

            // Configure the fetcher to run on a regular basis
            AutoFetchTimer.AutoReset = true;
            AutoFetchTimer.Elapsed += (sender, e) => {
                Fetch();
            };
            AutoFetchTimer.Interval = 5 * 60 * 1000;
        }

        /// <returns>null, as is required by the MSDN documentation.</returns>
        public System.Xml.Schema.XmlSchema GetSchema() {
            return null;
        }

        /// <summary>
        /// Deserializes a Calendar.
        /// </summary>
        /// <param name="reader"></param>
        public void ReadXml(System.Xml.XmlReader reader) {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

            // Sanity check for document structure
            if (!reader.Name.Equals("Calendar") || reader.NodeType != System.Xml.XmlNodeType.Element)
                throw new SettingsImportException(this, "Expected XML node <Calendar>, got <" + reader.Name + ">");

            // Parse all the calendar properties
            XmlDocument doc = new XmlDocument();
            XmlNode calNode = doc.ReadNode(reader);
            foreach (XmlNode node in calNode.ChildNodes)
            {
                if (node.Name == "DataUrl")
                    DataUrl = node.InnerText;
                else if (node.Name == "DefaultReminderEnabled")
                    DefaultReminderEnabled = bool.Parse(node.InnerText);
                else if (node.Name == "DefaultReminderOffset")
                    DefaultReminderOffset = long.Parse(node.InnerText);
            }

            // Check if we have all the mandatory stuff
            if (DataUrl == null)
                throw new SettingsImportException(this, "Data URL for this calendar is missing in XML source");
        }

        /// <summary>
        /// Serializes the current Calendar instance.
        /// </summary>
        public void WriteXml(System.Xml.XmlWriter writer) {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
            writer.WriteElementString("DataUrl", DataUrl);
            writer.WriteElementString("DefaultReminderEnabled", DefaultReminderEnabled.ToString());
            writer.WriteElementString("DefaultReminderOffset", DefaultReminderOffset.ToString());
        }

        /// <summary>
        /// Raises a specified PropertyChanged event.
        /// </summary>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Triggers an asynchronous calendar (re-)download. This call is non-blocking,
        /// so monitor the Fetched event to know when it's ready. The fetch request will be ignored
        /// if a fetch request is already underway.
        /// </summary>
        public void Fetch() {
            // Schedule a fetch request on the creating thread to avoid collisions.
            CreatorDispatcher.Invoke((Action) (() => {
                AutoFetchTimer.Stop();
                if (!Fetcher.IsBusy)
                {
                    Fetcher.RunWorkerAsync();
                    NotifyPropertyChanged("Status");
                }
            }));
        }

        /// <summary>
        /// (Re-)downloads the calendar. To be used in conjunction with a
        /// BackgroundWorker.
        /// </summary>
        private void FetchBackground(Object sender, DoWorkEventArgs args) {
            String oldHash = Cache.MD5Hash;
            bool oldOnlineStatus = Online;

            // Download the ICS file and let the cache update itself. If
            // nothing changed, return without sending out event notifications.
            try {
                String calFile = new System.Net.WebClient().DownloadString(DataUrl);
                bool updated = Cache.ParseICS(calFile);
                if (!updated) {
                    args.Result = false;
                    return;
                }
                Online = true;
            } catch (Exception) {
                Online = false;
                throw;
            }

            // Set result to true if a calendar update notification should go out.
            // This happens when the online status changes and/or the MD5 hash changed.
            args.Result = (!Cache.MD5Hash.Equals(oldHash) || oldOnlineStatus != Online);
        }


        /// <summary>
        /// Completion procedure for calendar (re-)download. Use in conjunction
        /// with a BackgroundWorker.
        /// </summary>
        private void FetchCompleted(Object sender, RunWorkerCompletedEventArgs e) {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
            NotifyPropertyChanged("Status");

            // Restart the autofetch timer
            AutoFetchTimer.Start();

            // Got an exception during execution? Pass it on.
            if (e.Error != null)
            {
                // Flag the event
                if (FetchFailed != null)
                    FetchFailed(this, new ErrorEventArgs(e.Error));
                return;
            }

            // Normal execution? Raise CalendarDownload event if needed.
            if (e.Result.GetType() == typeof(bool)) {
                LastDownloadTime = SystemClock.Instance.Now;
                bool raiseUpdate = (bool)e.Result;

                // Update the time when the last change occurred and notify
                // observers if necessary.
                if (raiseUpdate)
                {
                    LastChangeTime = SystemClock.Instance.Now;

                    if (Fetched != null)
                        Fetched(this, EventArgs.Empty);
                }
            }
        }


        /// <summary>
        /// Instructs ICS cache to re-evaluate which events are currently
        /// going on. Does not download a new version of the ICS source!
        /// Will spawn a BackgroundWorker and doesn't block. Should only
        /// be called on the thread that created this instance. Calls to
        /// trigger a cache update will be ignored if a cache update is
        /// already in progress.
        /// </summary>
        public void UpdateCache() {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

            lock (CacheUpdater) {
                if (!CacheUpdater.IsBusy) {
                    CacheUpdater.RunWorkerAsync();
                }
            }
        }

        /// <summary>
        /// Runs the cache updater. To be used with a BackgroundWorker.
        /// </summary>
        private void UpdateCacheBackground(object sender, DoWorkEventArgs e)
        {
            bool foundChange = Cache.Reevaluate();
            e.Result = foundChange;
        }

        /// <summary>
        /// Completion procedure for the cache updater. To be used with a
        /// BackgroundWorker.
        /// </summary>
        private void UpdateCacheCompleted(object sender, RunWorkerCompletedEventArgs e) {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
            Debug.Assert(e.Result.GetType() == typeof(bool));
            bool foundChange = (bool)e.Result;

            // If a change was detected during the calendar update
            // and someone is listening to this event, notify them.
            if (foundChange && CacheUpdated != null)
                CacheUpdated(this, EventArgs.Empty);
        }

        /// <summary>
        /// Returns a string-based representation of a certain instant.
        /// </summary>
        private String GetInformalInstant(Instant ins)
        {
            if (ins.Ticks == 0)
                return "Never";
            else
            {
                // Return a short regionalized date string
                DateTimeZone dtzLocal = DateTimeZoneProviders.Tzdb.GetSystemDefault();
                DateTimeOffset dtoStamp = ins.InZone(dtzLocal).ToDateTimeOffset();
                String date = dtoStamp.ToString("d");
                String time = dtoStamp.ToString("T");
                return date + " " + time;
            }
        }
    }
}
