﻿using Buzzer.GeneralExceptions;
using Buzzer.Structures;
using NodaTime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Buzzer.Model.ICSTools
{
    /// <summary>
    /// Iterates over an ICSEventBundle.
    /// </summary>
    public class ICSEventBundleEnumerator : IEnumerator
    {
        private ICSEventBundle EventBundle;

        // Internal scorekeeping
        private ICSEvent CurEvent;
        private SortedMultiListEnumerator<Instant, ICSEvent> RegularEnumerator;
        private SortedMultiListEnumerator<LocalDate, ICSEvent> DaywideEnumerator;

        /// <summary>Generic iterator value.</summary>
        object IEnumerator.Current
        {
            get { return CurEvent; }
        }

        /// <summary>Typed iterator value.</summary>
        public ICSEvent Current
        {
            get { return CurEvent; }
        }

        public ICSEventBundleEnumerator(ICSEventBundle eb)
        {
            EventBundle = eb;
            Reset();
        }

        /// <summary>Rewinds the enumerator.</summary>
        public void Reset()
        {
            RegularEnumerator = EventBundle.RegularEvents.GetEnumerator();
            RegularEnumerator.MoveNext();
            DaywideEnumerator = EventBundle.DaywideEvents.GetEnumerator();
            DaywideEnumerator.MoveNext();
        }

        /// <summary>Advances the enumerator.</summary>
        public bool MoveNext()
        {
            // Easiest case: both lists are exhausted
            if (!RegularEventsLeft() && !DaywideEventsLeft())
                return false;

            // One list is exhausted: advance the other one
            if (!RegularEventsLeft())
            {
                CurEvent = DaywideEnumerator.Current.Value;
                DaywideEnumerator.MoveNext();
                return true;
            }
            else if (!DaywideEventsLeft())
            {
                CurEvent = RegularEnumerator.Current.Value;
                RegularEnumerator.MoveNext();
                return true;
            }

            // Hardest case: two events are competing to be the next element.
            // If two events have the same date, always pick the daywide event first.
            LocalDate ldRegular = RegularEnumerator.Current.Key.InZone(DateTimeZoneProviders.Tzdb.GetSystemDefault()).Date;
            if (DaywideEnumerator.Current.Key <= ldRegular)
            {
                CurEvent = DaywideEnumerator.Current.Value;
                DaywideEnumerator.MoveNext();
            }
            else
            {
                CurEvent = RegularEnumerator.Current.Value;
                RegularEnumerator.MoveNext();
            }

            return true;
        }

        /// <summary>Checks if there are any regular events left to iterate over.</summary>
        private bool RegularEventsLeft()
        {
            try
            {
                KeyValuePair<Instant, ICSEvent> pair = RegularEnumerator.Current;
                return true;
            }
            catch (EnumerationException)
            {
                return false;
            }
        }

        /// <summary>Checks if there are any daywide events left to iterate over.</summary>
        private bool DaywideEventsLeft()
        {
            try
            {
                KeyValuePair<LocalDate, ICSEvent> pair = DaywideEnumerator.Current;
                return true;
            }
            catch (EnumerationException)
            {
                return false;
            }
        }
    }
}
