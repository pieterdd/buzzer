﻿using NodaTime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Model.ICSTools.Recurrence
{
    /// <summary>
    /// Implements iCal's FREQ=DAILY recurrence pattern.
    /// </summary>
    public class DailyRecurrence : RecurrenceGenerator
    {
        public DailyRecurrence(ZonedDateTime dtBase, RecurrenceParams recParams, List<RecurrenceGenerator> excludePatterns = null, int? count = null, ZonedDateTime? untilDT = null)
            : base(dtBase, recParams, excludePatterns, count, untilDT)
        {
        }

        public DailyRecurrence(ZonedDateTime dtBase, RecurrenceParams recParams, RecurrenceGenerator excludePattern, int? count = null, ZonedDateTime? untilDT = null)
            : base(dtBase, recParams, excludePattern, count, untilDT)
        {
        }

        protected override void AddNextOccurrence()
        {
            throw new NotImplementedException("TODO");
        }
    }
}
