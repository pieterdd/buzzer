﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Buzzer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            this.InitializeComponent();

            DateTime compileStamp = RetrieveLinkerTimestamp();
            String majorVersion = compileStamp.Year + compileStamp.Month.ToString().PadLeft(2, '0') + compileStamp.Day.ToString().PadLeft(2, '0');
            String minorVersion = compileStamp.Hour.ToString();
            Application.Current.Resources["AppTitle"] += " v" + majorVersion + "." + minorVersion;
#if DEBUG
            Application.Current.Resources["AppTitle"] += " [DEBUG]";
#endif
        }

        /// <summary>
        /// Retrieves the compile date for this build. (Credit:
        /// https://stackoverflow.com/a/1600990/217649)
        /// </summary>
        /// <returns></returns>
        private DateTime RetrieveLinkerTimestamp()
        {
            string filePath = System.Reflection.Assembly.GetCallingAssembly().Location;
            const int c_PeHeaderOffset = 60;
            const int c_LinkerTimestampOffset = 8;
            byte[] b = new byte[2048];
            System.IO.Stream s = null;

            try
            {
                s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                s.Read(b, 0, 2048);
            }
            finally
            {
                if (s != null)
                {
                    s.Close();
                }
            }

            int i = System.BitConverter.ToInt32(b, c_PeHeaderOffset);
            int secondsSince1970 = System.BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0);
            dt = dt.AddSeconds(secondsSince1970);
            dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours);
            return dt;
        }
    }
}
