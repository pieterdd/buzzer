﻿using Buzzer.Structures;
using NodaTime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buzzer;
using System.Diagnostics;

namespace Buzzer.Model.ICSTools
{
    /// <summary>
    /// Simple data structure that facilitates delivery of event bundles
    /// from the model to the UI. Notice how recurring events are missing here.
    /// If you need to report an occurrence of recurring event to the UI, create
    /// an ICSEvent for the particular occurrence of this event that you're referring to.
    /// </summary>
    [DebuggerDisplay("{RegularEvents.Count} regular(s), {DaywideEvents.Count} daywide(s)")]
    public class ICSEventBundle : IEnumerable
    {
        /// <summary>Events that aren't anchored to a whole day. This structure can contain newly
        /// ongoing regular events as well as reminders for upcoming regular events. The key
        /// is start time for newly ongoing events and reminder time for upcoming events.</summary>
        public readonly SortedMultiList<Instant, ICSEvent> RegularEvents;

        /// <summary>Events that are anchored to a whole day.</summary>
        public readonly SortedMultiList<LocalDate, ICSEvent> DaywideEvents;

        /// <summary>Shortcut that checks if there are any events in the bundle.</summary>
        public bool IsEmpty
        {
            get
            {
                return (RegularEvents.Count <= 0 && DaywideEvents.Count <= 0);
            }
        }

        /// <summary>
        /// Creates an empty event bundle.
        /// </summary>
        public ICSEventBundle()
        {
            RegularEvents = new SortedMultiList<Instant, ICSEvent>();
            DaywideEvents = new SortedMultiList<LocalDate, ICSEvent>();
        }

        /// <summary>
        /// Constructor for regular use.
        /// </summary>
        public ICSEventBundle(SortedMultiList<Instant, ICSEvent> timebasedEvents, SortedMultiList<LocalDate, ICSEvent> daywideEvents)
        {
            RegularEvents = timebasedEvents;
            DaywideEvents = daywideEvents;
        }

        /// <summary>Returns a generic enumerator.</summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        /// <summary>Returns a typed enumerator.</summary>
        public ICSEventBundleEnumerator GetEnumerator()
        {
            return new ICSEventBundleEnumerator(this);
        }

        /// <summary>
        /// Empties this event bundle.
        /// </summary>
        public void ClearAll()
        {
            RegularEvents.Clear();
            DaywideEvents.Clear();
        }

        /// <summary>
        /// Removes any events from this bundle that match up with an event from the
        /// supplied event bundle.
        /// </summary>
        public void RemoveIfPartOf(ICSEventBundle theirBundle)
        {
            RegularEvents.RemoveIfPartOf(theirBundle.RegularEvents);
            DaywideEvents.RemoveIfPartOf(theirBundle.DaywideEvents);
        }
    }
}
