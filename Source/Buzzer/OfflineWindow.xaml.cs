﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Buzzer.Notifications
{
	/// <summary>
	/// Shown when a calendar is currently unreachable.
	/// </summary>
	public partial class OfflineWindow : Window
	{
        /// <summary>
        /// The storyboard that is used to coordinate the countdown animation
        /// in the progress bar.
        /// </summary>
        private Storyboard CountdownSB = new Storyboard();

        /// <summary>
        /// ID of the thread that created this instance.
        /// </summary>
        private readonly int CreatorID;

		public OfflineWindow(String rawCalName)
		{
            CreatorID = Environment.CurrentManagedThreadId;

            // GUI init
			InitializeComponent();
            CalName.Text = rawCalName;

            // Storyboard setup
            CountdownSB.Completed += (obj, e) => {
                CloseNotification();
            };
            Storyboard.SetTarget(CountdownSB, UICountdown);
            Storyboard.SetTargetProperty(CountdownSB, new PropertyPath(ProgressBar.ValueProperty));

            // Start the close timer.
            StartCountdown();
		}

        /// <summary>
        /// Closes the window on the GUI thread when the closing timer expires.
        /// </summary>
        private void CloseNotification(object sender = null, EventArgs e = null)
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

            // Do a fadeout animation
            var anim = new DoubleAnimation(0, TimeSpan.FromMilliseconds(GlobalConstants.NfyFadeOutDuration));
            anim.Completed += (s, _) => Close();
            BeginAnimation(UIElement.OpacityProperty, anim);
        }

        /// <summary>
        /// Starts the progress bar animation that counts down to the closing
        /// of the notification window.
        /// </summary>
        private void StartCountdown()
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

            // Clear the storyboard of animations
            CountdownSB.Children.Clear();

            // Construct the new animation and add it to the storyboard
            TimeSpan duration = TimeSpan.FromSeconds(GlobalConstants.NfyDisplayDuration);
            DoubleAnimation animation = new DoubleAnimation(0, duration);
            animation.From = UICountdown.Maximum;
            CountdownSB.Children.Add(animation);

            // Start the animation
            CountdownSB.Begin();
        }

        /// <summary>
        /// Executed when a mouse enters the window.
        /// </summary>
        private void ProcessMouseEnter(object sender, MouseEventArgs e)
        {
            // Pause the countdown animation
            CountdownSB.Pause();
        }

        /// <summary>
        /// Executed when a mouse leaves the window.
        /// </summary>
        private void ProcessMouseLeave(object sender, MouseEventArgs e)
        {
            // Resume the countdown animation
            CountdownSB.Resume();
        }
	}
}