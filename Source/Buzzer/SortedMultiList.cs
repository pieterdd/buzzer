﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Structures
{
    /// <summary>
    /// Thin wrapper around SortedList<T, U> that lets us add
    /// new items more easily.
    /// </summary>

    public class SortedMultiList<T, U> : SortedList<T, List<U>>, IEnumerable
    {
        /// <summary>
        /// Convenience function that adds (key, value) pair without having
        /// to check if the list for key has already been allocated.
        /// </summary>
        public void Add(T key, U value)
        {
            if (!ContainsKey(key))
                this[key] = new List<U>();
            this[key].Add(value);
        }

        /// <summary>
        /// Convenience function that imports every element from
        /// a given source.
        /// </summary>
        public void AddAll(SortedMultiList<T, U> source)
        {
            foreach (KeyValuePair<T, U> curPair in source)
            {
                Add(curPair.Key, curPair.Value);
            }
        }

        /// <summary>
        /// Returns a generic iterator for this list.
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }


        /// <summary>
        /// Returns an iterator for this list.
        /// </summary>
        new public SortedMultiListEnumerator<T, U> GetEnumerator()
        {
            return new SortedMultiListEnumerator<T, U>(this);
        }

        /// <summary>
        /// Removes any key/value pair from this list that also appears
        /// in the supplied list.
        /// </summary>
        /// <param name="theirs"></param>
        public void RemoveIfPartOf(SortedMultiList<T, U> theirs)
        {
            // Work off a shallow copy of the current key list to avoid problems
            // with the enumerator while we're modifying the underlying data structure.
            List<T> ourKeys = new List<T>(Keys);
            foreach (T key in ourKeys)
            {
                // If our bundle and their bundle share a key, remove any values from
                // our key that also appear in theirs. If no values are left, don't
                // reinsert the key.
                if (theirs.ContainsKey(key))
                {
                    List<U> newValues = this[key].Except(theirs[key]).ToList();
                    Remove(key);
                    if (newValues.Count > 0)
                        this[key] = newValues;
                }
            }
        }
    }
}
