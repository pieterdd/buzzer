﻿using Buzzer.Model.ICSTools.Recurrence;
using System.Collections.Generic;

namespace Buzzer.Model.ICSTools.Recurrence
{
    /// <summary>
    /// Coordinates iteration over recurrence parameters for
    /// recurrence generators.
    /// </summary>
    public class RecurrenceParamIterator
    {
        private RecurrenceParams RecParams;

        private bool EnumerateYearDays;
        private List<int> YearDays;
        private bool EnumerateMonths;
        private List<int> Months;
        private bool EnumerateWeekNos;
        private List<int> WeekNos;
        private bool EnumerateDays;
        private List<int> Days;

        private bool EnumerateHours;
        private List<int> Hours;
        private bool EnumerateMinutes;
        private List<int> Minutes;
        private bool EnumerateSeconds;
        private List<int> Seconds;

        public RecurrenceParamIterator(RecurrenceParams recParams, bool enumerateYearDays = true, bool enumerateMonths = true, bool enumerateWeekNos = true,
            bool enumerateDays = true, bool enumerateHours = true, bool enumerateMinutes = true, bool enumerateSeconds = true)
        {
            // TODO: import list objects
            RecParams = recParams;

            EnumerateYearDays = enumerateYearDays;
            EnumerateMonths = enumerateMonths;
            EnumerateWeekNos = enumerateWeekNos;
            EnumerateDays = enumerateDays;

            EnumerateHours = enumerateHours;
            EnumerateMinutes = enumerateMinutes;
            EnumerateSeconds = enumerateSeconds;
        }

        // TODO: iteration functionality
    }
}
