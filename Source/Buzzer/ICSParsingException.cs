﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Model.ICSTools.Exceptions {
    /// <summary>
    /// Exceptions pertaining to the parsing of ICS data.
    /// </summary>
    public class ICSParsingException : Exception {
        public ICSElement Element {
            get;
            private set;
        }

        /// <param name="reason">Specific info about the nature of the error.</param>
        /// <param name="item">(Optional) The ICSItem that produced the error.</param>
        public ICSParsingException(String reason, ICSElement el = null)
            : base(reason)
        {
            Element = el;
        }
    }
}
