﻿using Buzzer.Model.ICSTools.Exceptions;
using Buzzer.Structures;
using NodaTime;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Model.ICSTools
{
    /// <summary>
    /// Helps us decompose an abstract ICS tree into events.
    /// </summary>
    public class ICSParser
    {
        /// <summary>Root of the ICS tree that is to be imported.</summary>
        private ICSElement Root;

        /// <summary>The name of this ICS tree. Will be empty before parsing.</summary>
        public String ICSName
        {
            get;
            private set;
        }

        /// <summary>List of events that were extracted from the tree. This can be
        /// null before parsing has taken place.</summary>
        public ICSEventBundle ImportedEvents
        {
            get;
            private set;
        }

        public ICSParser(ICSElement root)
        {
            Root = root;
            ICSName = "";
            ImportedEvents = new ICSEventBundle();
        }

        /// <summary>
        /// Initiates and executes the parsing process.
        /// </summary>
        public void Parse()
        {
            ICSName = Root.Properties["X-WR-CALNAME"];
            ProcessICSElements(Root.NestedElements, ImportedEvents.RegularEvents, ImportedEvents.DaywideEvents);
        }

        /// <summary>
        /// Processes a list of ICSElement objects into ICSEvents and sorts them
        /// into a relevant data structure.
        /// </summary>
        private void ProcessICSElements(List<ICSElement> els, SortedMultiList<Instant, ICSEvent> newRegEv, SortedMultiList<LocalDate, ICSEvent> newDWEv)
        {
            foreach (ICSElement el in els)
            {
                // Type filter: only do VEVENTs for now, ignore the rest
                if (el.Type.Equals("VEVENT"))
                {
                    // See if this will bring us a valid VEVENT
                    ICSEvent ev = ParseVEVENT(el);
                    if (ev == null)
                        continue;

                    // Sort the ICSEvent into the right bin
                    if (!ev.Daywide)
                        newRegEv.Add(ev.Start, ev);
                    else
                        newDWEv.Add(ev.StartDate, ev);
                }
            }
        }

        /// <summary>
        /// Takes a VEVENT element from the tree and converts it to an object we
        /// understand. Function will return null on invalid or expired event.
        /// 
        /// Doesn't support recurring events yet! (TODO)
        /// </summary>
        private ICSEvent ParseVEVENT(ICSElement el)
        {
            Instant now = SystemClock.Instance.Now;
            Debug.Assert(el.Type.Equals("VEVENT"));

            // General information
            Instant start = ParseDateTime(el.Properties["DTSTART"]);
            Instant end = new Instant(start.Ticks);
            if (el.Properties.ContainsKey("DTEND"))
                end = ParseDateTime(el.Properties["DTEND"]);
            if (end < now)
                return null;
            String summary = el.Properties["SUMMARY"];
            summary = summary.Replace("\\,", ",");
            bool dayWide = (el.Properties["DTSTART"].IndexOf("VALUE=DATE") != -1);

            // Import any alarms that were set up for this event
            List<Instant> alarms = new List<Instant>();
            for (int i = 0; i < el.NestedElements.Count; ++i)
            {
                try
                {
                    if (el.NestedElements[i].Type.Equals("VALARM"))
                        alarms.Add(ParseAlarmTrigger(el.NestedElements[i], start));
                }
                catch (ICSParsingException)
                {
                    // Swallow incomplete/illegible alarm elements
                    continue;
                }
            }

            ICSEvent ev = new ICSEvent(summary, start, end, dayWide, alarms);
            return ev;
        }

        /// <summary>
        /// Parses a VALARM declaration relative to the starting time of
        /// the associated event.
        /// </summary>
        private Instant ParseAlarmTrigger(ICSElement alarmEl, Instant startTime)
        {
            // Check if the TRIGGER property is available
            if (!alarmEl.Properties.ContainsKey("TRIGGER"))
                throw new ICSParsingException("TRIGGER not set on alarm element", alarmEl);

            // Extract and decode it
            String triggerValue = alarmEl.Properties["TRIGGER"];
            int timeShiftStartPos = triggerValue.IndexOf("T");
            if (timeShiftStartPos == -1 || timeShiftStartPos + 1 >= triggerValue.Length)
                throw new ICSParsingException("Alarm trigger could not be deciphered", alarmEl);

            // Get the offset and compose the trigger time
            Period offset = ParseTriggerOffset(triggerValue.Substring(timeShiftStartPos + 1));
            Duration dOffset = offset.ToDuration();
            if (triggerValue.StartsWith("-"))
                return startTime.Minus(dOffset);
            else
                return startTime.Plus(dOffset);
        }

        /// <summary>
        /// Deciphers a trigger offset of an alarm.
        /// </summary>
        private Period ParseTriggerOffset(String triggerCode)
        {
            PeriodBuilder pb = new PeriodBuilder();
            String buffer = "";
            for (int i = 0; i < triggerCode.Length; ++i)
            {
                // Digit? Just add it to the buffer and move on.
                if (triggerCode[i] >= '0' && triggerCode[i] <= '9')
                {
                    buffer += triggerCode[i];
                    continue;
                }

                // Got one of the delimiters?
                if (triggerCode[i] == 'H')
                    pb.Hours = Int32.Parse(buffer);
                else if (triggerCode[i] == 'M')
                    pb.Minutes = Int32.Parse(buffer);
                else if (triggerCode[i] == 'S')
                    pb.Seconds = Int32.Parse(buffer);
                // We're giving up.
                else
                    throw new ICSParsingException("Found unexpected character while parsing alarm trigger");

                // Anything that made it down here should trigger a buffer reset.
                buffer = "";
            }

            // Get the offset
            return pb.Build();
        }

        /// <summary>
        /// Parses the value of a DTSTART or DTEND attribute. Returns the date/time
        /// in a timezone-independent data structure.
        /// </summary>
        private Instant ParseDateTime(String icsDateTime)
        {
            // Get the timezone
            String timezone = "UTC";
            int tzPos = icsDateTime.IndexOf("TZID=");
            int splitPos = icsDateTime.IndexOf(":");
            if (tzPos != -1)
                timezone = icsDateTime.Substring(tzPos + 5, splitPos - 5 - tzPos);

            // Get the raw date/time stamp
            String rawDateTime = icsDateTime;
            if (splitPos != -1)
                rawDateTime = rawDateTime.Substring(splitPos + 1);

            // Get the timestamp. Don't account for timezone yet.
            DateTime unshiftedTimestamp = ParseDateTimeStamp(rawDateTime);

            // Regular event require that the timestamp is shifted according to its timezone.
            if (icsDateTime.IndexOf("VALUE=DATE") == -1)
            {
                LocalDateTime ldt = LocalDateTime.FromDateTime(unshiftedTimestamp);
                DateTimeZone tzone = DateTimeZoneProviders.Tzdb[timezone];
                Instant shiftedTimestamp = ldt.InZoneStrictly(tzone).ToInstant();
                return shiftedTimestamp;
            }

            // Timestamps from daywide events don't need to be adjusted.
            DateTime dateStamp = new DateTime(unshiftedTimestamp.Ticks, DateTimeKind.Utc);
            return Instant.FromDateTimeUtc(dateStamp);
        }

        /// <summary>
        /// Parses a raw ICS timestamp (i.e. no timezone info) and returns it
        /// in DateTime form.
        /// </summary>
        private DateTime ParseDateTimeStamp(String plainDateTime)
        {
            String year = plainDateTime.Substring(0, 4);
            String month = plainDateTime.Substring(4, 2);
            String day = plainDateTime.Substring(6, 2);

            // Not all events have set an explicit time
            String hour = "00";
            String minute = "00";
            String second = "00";
            if (plainDateTime.Length > 8)
            {
                hour = plainDateTime.Substring(9, 2);
                minute = plainDateTime.Substring(11, 2);
                second = plainDateTime.Substring(13, 2);
            }

            DateTime dateTime = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day),
                Convert.ToInt32(hour), Convert.ToInt32(minute), Convert.ToInt32(second));
            return dateTime;
        }
    }
}
