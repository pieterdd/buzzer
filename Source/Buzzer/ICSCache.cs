﻿using Buzzer.GeneralExceptions;
using Buzzer.Model.ICSTools.Exceptions;
using Buzzer.Structures;
using NodaTime;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Buzzer.Model.ICSTools
{
    /// <summary>
    /// Parses raw ICS files and caches the resulting data so that
    /// it may be used by outside sources. Class is thread-safe.
    /// </summary>
    class ICSCache
    {
        static HashSet<String> KeywordFilter = new HashSet<string>(new String[] {"X-WR-CALNAME",
                "DTSTART", "DTEND", "SUMMARY", "LAST-MODIFIED", "TRIGGER"});
        private ICSTreeBuilder Itb = new ICSTreeBuilder(KeywordFilter);
        public String Name
        {
            // This field is atomic. See http://bit.ly/csharpatomicity.
            get;
            private set;
        }
        private Object SyncLock = new Object();
        public String MD5Hash
        {
            get { return Itb.MD5Hash; }
        }

        /// <summary>
        /// The default Period-typed offset reminder time for events that don't have any
        /// alarms set up. Don't change this variable directly!
        /// </summary>
        private Period DefaultReminderPeriod;

        /// <summary>
        /// Whether default reminders for events without reminders are enabled. This field
        /// is atomic. See http://bit.ly/csharpatomicity.
        /// </summary>
        public bool DefaultReminderEnabled
        {
            get { return INTERNAL_DefaultReminderEnabled; }
            set
            {
                // Rebuild the reminder cache following this change
                EngageSyncLock(-1);
                INTERNAL_DefaultReminderEnabled = value;
                RebuildReminderCache();
                Monitor.Exit(SyncLock);
            }
        }
        private bool INTERNAL_DefaultReminderEnabled = true; // Don't touch this directly!

        /// <summary>
        /// The default reminder offset time in minutes for events that don't have any
        /// alarms set up.
        /// </summary>
        public long DefaultReminderOffset
        {
            get {
                Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
                Debug.Assert(DefaultReminderPeriod != null);
                return DefaultReminderPeriod.Minutes;
            }

            set
            {
                Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

                // Update reminder offset and rebuild the reminder cache
                PeriodBuilder pb = new PeriodBuilder();
                pb.Minutes = value;
                DefaultReminderPeriod = pb.Build();
                EngageSyncLock(-1);
                RebuildReminderCache();
                Monitor.Exit(SyncLock);
            }
        }

        // Events that will start at some point in the future. Key is start time. Daywide events
        // aren't UTC'ed and are always in local time.
        private SortedMultiList<Instant, ICSEvent> RegularEvents = new SortedMultiList<Instant, ICSEvent>();
        private SortedMultiList<LocalDate, ICSEvent> DaywideEvents = new SortedMultiList<LocalDate, ICSEvent>();

        /// <summary>
        /// Stores future reminders. Key is reminder time; value is the event that the reminder
        /// was set for.
        /// </summary>
        private SortedMultiList<Instant, ICSEvent> Reminders = new SortedMultiList<Instant, ICSEvent>();

        // Events that are ongoing. Key is end time. Daywide events
        // aren't UTC'ed and are always in local time.
        private SortedMultiList<Instant, ICSEvent> ActiveRegularEvents = new SortedMultiList<Instant, ICSEvent>();
        private SortedMultiList<LocalDate, ICSEvent> ActiveDaywideEvents = new SortedMultiList<LocalDate, ICSEvent>();

        /// This is a list of events that became active during the last event reevaluation. Can be
        /// useful to display a notification when there has been a change.
        public ICSEventBundle NewlyActiveEvents
        {
            get;
            private set;
        }

        /// <summary>
        /// ID of the creating thread.
        /// </summary>
        private readonly int CreatorID;

        /// <summary>
        /// Bundle of events that are banned from appearing in the newly active events bundle.
        /// This data structure is used to prevent active events from reappearing as a newly
        /// ongoing event after the calendar has been redownloaded.
        /// </summary>
        private ICSEventBundle NewlyActiveBlacklist = new ICSEventBundle();

        public ICSCache()
        {
            Name = "Unknown Calendar";
            CreatorID = Environment.CurrentManagedThreadId;
            NewlyActiveEvents = new ICSEventBundle();

            // Set default reminder time
            PeriodBuilder pb = new PeriodBuilder();
            pb.Minutes = 5;
            DefaultReminderPeriod = pb.Build();
        }

        /// <summary>
        /// Parses raw ICS data. Only the data from the most recent ICS parse task is kept.
        /// </summary>
        /// <param name="msecTimeout">(Optional) Time to wait on the sync lock.</param>
        /// <returns>true if the cache was out of date.</returns>
        public bool ParseICS(String rawData, int msecTimeout = -1)
        {
            EngageSyncLock(msecTimeout);

            // Only flush the cache if the ICS file changed.
            bool updatedTree = Itb.BuildICSTree(rawData);
            if (!updatedTree)
            {
                Monitor.Exit(SyncLock);
                return false;
            }

            // Prevent currently active events from reappearing on the list of newly ongoing events.
            NewlyActiveBlacklist = new ICSEventBundle(ActiveRegularEvents, ActiveDaywideEvents);

            // Parse the ICS tree that was just built
            ICSParser parser = new ICSParser(Itb.RootElement);
            parser.Parse();
            Name = parser.ICSName;

            // Process reminders
            SortedMultiList<Instant, ICSEvent> newReminders = new SortedMultiList<Instant, ICSEvent>();
            ImportReminders(newReminders, parser.ImportedEvents.RegularEvents);

            // Replace the existing structures
            RegularEvents = parser.ImportedEvents.RegularEvents;
            ActiveRegularEvents = new SortedMultiList<Instant, ICSEvent>();
            DaywideEvents = parser.ImportedEvents.DaywideEvents;
            ActiveDaywideEvents = new SortedMultiList<LocalDate, ICSEvent>();
            Reminders = newReminders;

            Monitor.Exit(SyncLock);
            return true;
        }

        /// <summary>
        /// Fills a reminder cache by processing planned reminders. If a default reminder is set,
        /// events that don't have any reminders set up will be scheduled for a reminder at that time.
        /// Engage sync lock if you're supplying the reminder cache that is currently in use!
        /// </summary>
        private void ImportReminders(SortedMultiList<Instant, ICSEvent> newReminders, SortedMultiList<Instant, ICSEvent> newRegEv)
        {
            Debug.Assert(DefaultReminderPeriod != null);
            Instant now = SystemClock.Instance.Now;

            // Either do default reminders for everything or for nothing. Since DefaultReminderEnabled
            // can be modified by any thread, we're caching this field beforehand.
            bool setupDefaultReminders = DefaultReminderEnabled;

            // Go through all regular events
            foreach (KeyValuePair<Instant, ICSEvent> curPair in newRegEv)
            {
                ICSEvent ev = curPair.Value;

                // Default reminder rules?
                if (setupDefaultReminders && ev.Alarms.Count <= 0 && !ev.Daywide)
                {
                    Instant reminderTime = ev.Start.Minus(DefaultReminderPeriod.ToDuration());
                    if (now <= reminderTime)
                        newReminders.Add(reminderTime, ev);
                }
                // In other cases, import each planned reminder that hasn't passed yet.
                else
                {
                    foreach (Instant reminderTime in ev.Alarms)
                    {
                        if (now <= reminderTime)
                            newReminders.Add(reminderTime, ev);
                    }
                }
            }
        }

        /// <summary>
        /// Re-evaluates which events are currently ongoing.
        /// </summary>
        /// <returns>Whether a change was made, not including cleanup of passed events.</returns>
        public bool Reevaluate(int msecTimeout = -1)
        {
            Instant utcNow = FlooredTimestamp(SystemClock.Instance.Now);
            EngageSyncLock(msecTimeout);

            CleanPassedEvents(utcNow);
            bool foundChange = FetchNewlyOngoingEvents(utcNow);

            Monitor.Exit(SyncLock);
            return foundChange;
        }

        /// <summary>
        /// Rebuilds the reminder cache, taking into account current settings for default
        /// reminders. Engage the sync lock before calling this function!
        /// </summary>
        private void RebuildReminderCache()
        {
            Reminders.Clear();
            ImportReminders(Reminders, RegularEvents);
        }

        /// <summary>
        /// Cleans out ongoing events that have passed from the cache.
        /// </summary>
        /// <returns>Whether a change was made.</returns>
        private bool CleanPassedEvents(Instant utcNow)
        {
            bool foundChange = false;

            // Delete all ongoing events and reminders ending before now
            foundChange = RemovePassedEventsFromList(ActiveRegularEvents, utcNow);
            foundChange = RemovePassedDaywideEventsFromList(ActiveDaywideEvents, utcNow) || foundChange;

            return foundChange;
        }

        /// <summary>
        /// Removes events from a given list that have passed, according to a supplied reference time.
        /// </summary>
        /// <returns>Whether a change was made.</returns>
        private bool RemovePassedEventsFromList(SortedMultiList<Instant, ICSEvent> sl, Instant refTime)
        {
            bool foundChange = false;

            // Keep trimming the list until we start encountering events that haven't ended yet.
            while (sl.Count > 0)
            {
                KeyValuePair<Instant, List<ICSEvent>> curElement = sl.ElementAt(0);
                if (refTime < curElement.Key)
                    break;
                sl.RemoveAt(0);
                foundChange = true;
            }

            return foundChange;
        }

        /// <summary>
        /// Analogous to <seealso cref="ICSCache.RemovePassedEventsFromList"/>, but for daywide events. Also
        /// read <seealso cref="ICSEvent.Daywide"/> for further context.
        /// </summary>
        private bool RemovePassedDaywideEventsFromList(SortedMultiList<LocalDate, ICSEvent> sl, Instant refTime)
        {
            bool foundChange = false;
            DateTimeZone sysTz = NodaTime.DateTimeZoneProviders.Tzdb.GetSystemDefault();
            LocalDate refDate = refTime.InZone(sysTz).LocalDateTime.Date;

            // Keep trimming the list until we start encountering events that haven't ended yet.
            while (sl.Count > 0)
            {
                KeyValuePair<LocalDate, List<ICSEvent>> curElement = sl.ElementAt(0);
                if (refDate <= curElement.Key)
                    break;
                sl.RemoveAt(0);
                foundChange = true;
            }

            return foundChange;
        }

        /// <summary>
        /// Move newly ongoing events to the active event list. Engage sync lock
        /// before calling!
        /// </summary>
        /// <returns>Whether a change was made or reminders were encountered.</returns>
        private bool FetchNewlyOngoingEvents(Instant utcNow)
        {
            bool foundChange = false;

            // Find out which events will become active
            SortedMultiList<Instant, ICSEvent> reportedRegulars = new SortedMultiList<Instant, ICSEvent>();
            SortedMultiList<LocalDate, ICSEvent> reportedDWs = new SortedMultiList<LocalDate, ICSEvent>();
            foundChange = MoveNewlyOngoingEvents(RegularEvents, reportedRegulars, utcNow);
            foundChange = MoveNewlyOngoingDaywideEvents(DaywideEvents, reportedDWs, utcNow) || foundChange;

            // Add these events to the active list
            ActiveRegularEvents.AddAll(reportedRegulars);
            ActiveDaywideEvents.AddAll(reportedDWs);

            // Get any reminders that were planned during this minute and
            // wipe 'em from the reminder cache. We don't need them after this,
            // so let's save some precious memory!
            if (Reminders.ContainsKey(utcNow))
            {
                foundChange = true;
                foreach (ICSEvent ev in Reminders[utcNow])
                    reportedRegulars.Add(utcNow, ev);
                Reminders.Remove(utcNow);
            }
            
            // Register as the newest bundle of active events/reminders, but remove
            // blacklisted active events. The blacklist can be cleared afterwards.
            NewlyActiveEvents = new ICSEventBundle(reportedRegulars, reportedDWs);
            NewlyActiveEvents.RemoveIfPartOf(NewlyActiveBlacklist);
            NewlyActiveBlacklist.ClearAll();

            return foundChange;
        }

        /// <summary>
        /// Returns the supplied timestamp with its seconds and miliseconds removed.
        /// </summary>
        private Instant FlooredTimestamp(Instant refStamp)
        {
            // Convert the reference stamp to local time
            DateTimeZone localTz = DateTimeZoneProviders.Tzdb.GetSystemDefault();
            ZonedDateTime zdtRef = refStamp.InZone(localTz);

            // Construct a "clean" ZonedDateTime that doesn't get any more detailed than
            // the minute mark.
            LocalDateTime ldtFloored = new LocalDateTime(zdtRef.Year, zdtRef.Month, zdtRef.Day, zdtRef.Hour, zdtRef.Minute);
            ZonedDateTime zdtFloored = new ZonedDateTime(ldtFloored, localTz, localTz.GetUtcOffset(refStamp));
            return zdtFloored.ToInstant();
        }

        /// <summary>
        /// Moves events that are, as of a given reference time, considered ongoing
        /// from one list to another.
        /// </summary>
        /// <returns>Whether a change was made.</returns>
        private bool MoveNewlyOngoingEvents(SortedMultiList<Instant, ICSEvent> from,
            SortedMultiList<Instant, ICSEvent> to, Instant refTime)
        {
            bool foundChange = false;

            // Keep moving events until we encounter an event that hasn't started
            // yet. This works because the list is sorted chronologically.
            while (from.Count > 0)
            {
                KeyValuePair<Instant, List<ICSEvent>> curElement = from.ElementAt(0);
                List<ICSEvent> events = curElement.Value;

                // Is the start time in the future? Time to stop moving events.
                if (refTime < curElement.Key)
                    break;

                // If not, evaluate the bundle of events starting at this time.
                while (events.Count > 0)
                {
                    ICSEvent curEvent = events[0];

                    // If this event has started but not ended,
                    // move it to the active event list. Otherwise
                    // merely delete it.
                    if (curEvent.Start <= refTime && refTime < curEvent.End)
                        to.Add(curEvent.End, curEvent);
                    else
                        Debug.Assert(curEvent.End <= refTime);
                    events.RemoveAt(0);
                }

                // No point in keeping an empty list around
                foundChange = true;
                from.RemoveAt(0);
            }

            return foundChange;
        }

        /// <summary>
        /// Like <seealso cref="ICSCache.MoveNewlyOngoingEvents"/>, but for daywide events.
        /// </summary>
        private bool MoveNewlyOngoingDaywideEvents(SortedMultiList<LocalDate, ICSEvent> from,
            SortedMultiList<LocalDate, ICSEvent> to, Instant refTime)
        {
            bool foundChange = false;

            // What's the current date in the system's timezone?
            DateTimeZone sysTz = NodaTime.DateTimeZoneProviders.Tzdb.GetSystemDefault();
            LocalDate refDate = refTime.InZone(sysTz).LocalDateTime.Date;

            // Keep moving events until we encounter an event that hasn't started
            // yet. This works because the list is sorted chronologically.
            while (from.Count > 0)
            {
                KeyValuePair<LocalDate, List<ICSEvent>> curElement = from.ElementAt(0);
                List<ICSEvent> events = curElement.Value;

                // Is the start date in the future? Time to stop moving events.
                if (refDate < curElement.Key)
                    break;

                while (events.Count > 0)
                {
                    ICSEvent curEvent = events[0];

                    // If this event has started but not ended, move it to the 
                    // active event list. Otherwise merely delete it.
                    if (curEvent.StartDate <= refDate && refDate <= curEvent.EndDate)
                        to.Add(curEvent.EndDate, curEvent);
                    else
                        Debug.Assert(curEvent.EndDate < refDate);
                    events.RemoveAt(0);
                }

                foundChange = true;
                from.RemoveAt(0);
            }

            return foundChange;
        }

        /// <summary>
        /// Try to engage syncLock and wait at most msecTimeout in doing
        /// so. If this fails, throw an exception.
        /// </summary>
        private void EngageSyncLock(int msecTimeout)
        {
            bool lockAcquired = Monitor.TryEnter(SyncLock, msecTimeout);
            if (!lockAcquired)
                throw new LockInUseException(msecTimeout);
        }
    }
}
