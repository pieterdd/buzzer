﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.GeneralExceptions {
    /// <summary>
    /// Thrown when a task that requires a lock has to be
    /// cancelled because the lock wasn't freed within a 
    /// timeout window.
    /// </summary>
    public class LockInUseException : Exception {
        public int MsecsTimeout {
            get;
            private set;
        }

        public LockInUseException(int msecsTimeout)
            : base("Task cancelled because lock wasn't freed within " + msecsTimeout + " msecs.") {
            MsecsTimeout = msecsTimeout;
        }
    }
}
