﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer
{
    class GlobalConstants
    {
        /// <summary>
        /// Duration of notification fade-outs (in miliseconds).
        /// </summary>
        public static int NfyFadeOutDuration = 250;

        /// <summary>
        /// The number of seconds during which a notification is displayed
        /// on the screen (in seconds).
        /// </summary>
        public static double NfyDisplayDuration = 5;
    }
}
