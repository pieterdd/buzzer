﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Structures
{
    /// <summary>
    /// General-purpose list that rejects values outside a specified range.
    /// The list type needs to be comparable.
    /// </summary>
    public class RangedList<T> : List<T> where T: IComparable
    {
        /// <summary>
        /// The lowest permissable value that will be let into the list.
        /// </summary>
        public readonly T LowerBound;

        /// <summary>
        /// The highest permissable value that will be let into the list.
        /// The Equals operation will be used for comparisons, not operator==.
        /// </summary>
        public readonly T UpperBound;

        /// <summary>
        /// Other values that are not allowed into the list.
        /// </summary>
        public List<T> Exceptions;

        public RangedList(T lowerBound, T upperBound, List<T> exceptions = null)
        {
            if (lowerBound.CompareTo(upperBound) > 0)
                throw new ArgumentException("The lower bound of the ranged list should be less than or equal to the upper bound");
            LowerBound = lowerBound;
            UpperBound = upperBound;
            Exceptions = exceptions;
            if (Exceptions == null)
                Exceptions = new List<T>();
        }

        /// <summary>
        /// Adds an item to the list. If the item is outside the range of the
        /// list or the item is on the ban list, an exception will be thrown.
        /// </summary>
        /// <param name="item"></param>
        public new void Add(T item)
        {
            // First check the bounds
            if (item.CompareTo(LowerBound) < 0)
                throw new ArgumentException("Item falls below the lower bound and can't be added");
            else if (item.CompareTo(UpperBound) > 0)
                throw new ArgumentException("Item falls above the upper bound and can't be added");

            // Then verify that it isn't on the exclusion list
            foreach (T exception in Exceptions)
                if (exception.Equals(item))
                    throw new ArgumentException("Item is on the exclusion list and can't be added");

            base.Add(item);
        }
    }
}
