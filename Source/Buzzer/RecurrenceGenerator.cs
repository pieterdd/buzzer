﻿using NodaTime;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Model.ICSTools.Recurrence
{
    /// <summary>
    /// Abstract base class for the generators that implement ICS recurrence definitions.
    /// See http://www.kanzaki.com/docs/ical/recur.html for more information.
    /// </summary>
    public abstract class RecurrenceGenerator
    {
        /// <summary>
        /// Local date/time indicating the start of the repetition
        /// pattern.
        /// </summary>
        protected ZonedDateTime DTBase;

        /// <summary>
        /// A list of recurrence patterns representing the occurrences that are to be excluded.
        /// </summary>
        protected List<RecurrenceGenerator> ExcludePatterns = new List<RecurrenceGenerator>();

        /// <summary>
        /// A list of cached repetition occurrences with no guarantee of being exhaustive. We
        /// store this information to speed up repetition calculations by saving previously
        /// calculated repetitions.
        /// 
        /// We chose a SortedSet because its Contains() operation only has O(ln n) complexity,
        /// but duplicate entries are ignored and changes to the sort values of existing items
        /// is not supported. See http://bit.ly/sortedset.
        /// </summary>
        protected SortedSet<ZonedDateTime> OccurrenceCache = new SortedSet<ZonedDateTime>();

        /// <summary>
        /// The total number of recurrences, as specified by the COUNT property in ICS files.
        /// If set to null, we will assume an infinite recurrence unless <see cref="UntilDT"/>
        /// has been set.
        /// </summary>
        protected int? Count;

        /// <summary>
        /// The last date/time that is allowed into this recurrence pattern. Unless set to
        /// null, any occurrences happening after this date should be discarded.
        /// </summary>
        protected ZonedDateTime? UntilDT;

        /// <summary>
        /// Contains BYHOUR, BYDAY, INTERVAL and other recurrence parameters.
        /// </summary>
        protected RecurrenceParams RecParams;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="dtBase">See <see cref="RecurrenceGenerator.BaseInstant"/>.</param>
        /// <param name="excludePatterns">See <see cref="RecurrenceGenerator.ExcludePatterns"/>. Leave out
        /// if there are no exclusion patterns.</param>
        /// <param name="count">See <see cref="Count"/>. Defaults to -1, meaning infinite recurrence.</param>
        public RecurrenceGenerator(ZonedDateTime dtBase, RecurrenceParams recParams, List<RecurrenceGenerator> excludePatterns = null, int? count = null, ZonedDateTime? untilDT = null)
        {
            DTBase = dtBase;
            RecParams = recParams;
            if (excludePatterns != null)
                ExcludePatterns = excludePatterns;
            Count = count;
            UntilDT = untilDT;
        }

        /// <summary>
        /// Convenience constructor for patterns with only one recurrence pattern.
        /// </summary>
        public RecurrenceGenerator(ZonedDateTime dtBase, RecurrenceParams recParams, RecurrenceGenerator excludePattern, int? count = null, ZonedDateTime? untilDT = null)
        {
            DTBase = dtBase;
            RecParams = recParams;
            ExcludePatterns.Add(excludePattern);
            Count = count;
            UntilDT = untilDT;
        }

        /// <summary>
        /// Fills the occurrence cache until a certain reference time, with the inclusion of
        /// that reference time.
        /// </summary>
        protected void AppendCacheUntil(ZonedDateTime refDT)
        {
            // Keep adding new occurrences to the cache until we've reached the
            // reference date/time. If we reach the end of the recurrence pattern
            // by then, an exception will be thrown.
            while (GetLastCalculatedOccurrence() < refDT)
                AddNextOccurrence();
        }

        /// <summary>
        /// Returns the last occurrence that has been calculated as a starting point for
        /// further calculations. If no occurrences have been calculated yet, the base will
        /// be returned.
        /// </summary>
        protected ZonedDateTime GetLastCalculatedOccurrence()
        {
            if (OccurrenceCache.Count > 0)
                return OccurrenceCache.Last();
            return DTBase;
        }

        /// <summary>
        /// Checks if a certain date/time occurs is part of this repetition
        /// pattern. The reference date/time must be in the same timezone as
        /// the base to avoid unexpected glitches!
        /// </summary>
        public bool ContainsDateTime(ZonedDateTime refDT)
        {
            Debug.Assert(DTBase.Zone.Equals(refDT.Zone));

            // Calculate repetitions until this reference date/time if necessary
            AppendCacheUntil(refDT);

            return OccurrenceCache.Contains(refDT);
        }

        /// <summary>
        /// Convenience alias for the regular <see cref="ContainsDateTime"/> that
        /// takes an instant as a parameter. The instant will be interpreted in
        /// the timezone of the base date/time.
        /// </summary>
        /// <param name="refInstant"></param>
        /// <returns></returns>
        public bool ContainsDateTime(Instant refInstant)
        {
            return ContainsDateTime(refInstant.InZone(DTBase.Zone));
        }

        /// <summary>
        /// Checks if a certain date/time was explicitly excluded from this
        /// repetition pattern. The reference date/time must be in the same
        /// timezone as the base to avoid unexpected glitches!
        /// </summary>
        public bool ExcludesDateTime(ZonedDateTime refDT)
        {
            Debug.Assert(DTBase.Zone.Equals(refDT.Zone));

            // Examine each exclusion pattern and return immediately if we
            // find the instant.
            for (int exListID = 0; exListID < ExcludePatterns.Count; ++exListID)
                if (ExcludePatterns[exListID].ContainsDateTime(refDT))
                    return true;
            return false;
        }

        /// <summary>
        /// This function will add the next occurrence to the cache. If the
        /// end of the recurrence has been reached, this function will throw
        /// an exception.
        /// </summary>
        protected abstract void AddNextOccurrence();

        /// <summary>
        /// Returns the occurrence following the reference date/time. Will 
        /// throw an exception if there is no occurrence following the
        /// reference. Supplied reference date/time should be a part of
        /// this recurrence.
        /// </summary>
        public ZonedDateTime GetOccurrenceAfter(ZonedDateTime refDT)
        {
            Debug.Assert(ContainsDateTime(refDT));
            AppendCacheUntil(refDT);

            // Does the reference date/time occur equal the last cached
            // occurrence? Then we have to calculate one more occurrence
            // first. If there are no more occurrences left to add, an
            // exception should be thrown by AddNextOccurrences().
            if (OccurrenceCache.Last().Equals(refDT))
                AddNextOccurrence();
                
            IEnumerator<ZonedDateTime> nextEnumerator = OccurrenceCache.Where(p => refDT < p).GetEnumerator();
            return nextEnumerator.Current;
        }

        /// <summary>
        /// Convenience function for <see cref="GetOccurrenceAfter"/> that
        /// takes an Instant as argument. Time zone conversion will be taken
        /// care of behind the scenes. Will throw an exception if there is no
        /// occurrence following this instant.
        /// </summary>
        public Instant GetOccurrenceAfter(Instant refInstant)
        {
            ZonedDateTime result = GetOccurrenceAfter(refInstant.InZone(DTBase.Zone));
            return result.ToInstant();
        }
    }
}
