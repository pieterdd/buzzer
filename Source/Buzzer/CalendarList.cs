﻿using Buzzer.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Model
{
    /// <summary>
    /// Holds a list of calendars that can be observed in the UI.
    /// </summary>
    public class CalendarList : ObservableCollection<Calendar>
    {
        public CalendarList()
            : base()
        {
            CollectionChanged += ProcessCollectionChange;
        }

        private void ProcessCollectionChange(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            // Subscribe to events from new calendars
            if (e.NewItems != null)
            {
                foreach (Calendar cal in e.NewItems)
                {
                    cal.Fetched += ProcessCalChange;
                    cal.CacheUpdated += ProcessCalChange;
                }
            }
            // Unsubscribe to events from deleted calendars
            if (e.OldItems != null)
            {
                foreach (Calendar cal in e.OldItems)
                {
                    cal.Fetched -= ProcessCalChange;
                    cal.CacheUpdated -= ProcessCalChange;
                }
            }
        }

        /// <summary>
        /// Triggers the CollectionChanged event that notifies observers
        /// of the calendar list when a calendar changes internally.
        /// </summary>
        private void ProcessCalChange(object sender, EventArgs args)
        {
            NotifyCollectionChangedEventArgs a = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            OnCollectionChanged(a);
        }
    }
}
