﻿using Buzzer.Model.ICSTools;
using NodaTime;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Buzzer.View.Components
{
	/// <summary>
	/// Renders a pie-shaped indication of remaining time in an associated ICS event.
	/// </summary>
	public partial class EventProgress : UserControl
    {
        #region Dependency properties
        /// <summary>
        /// When updated, this control will reflect the state of this event.
        /// </summary>
        public ICSEvent CurEvent
        {
            get { return GetValue(CurEventProp) as ICSEvent; }
            set { SetValue(CurEventProp, value); }
        }
        public static DependencyProperty CurEventProp = DependencyProperty.Register("CurEvent", typeof(ICSEvent), typeof(EventProgress),
            new PropertyMetadata(null, OnCurEventChanged));
        #endregion

        public EventProgress()
		{
			this.InitializeComponent();
		}

        private static void OnCurEventChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = d as EventProgress;
            obj.UpdateView();
        }

        /// <summary>
        /// Re-renders the pie based on data that was pulled from the associated event.
        /// </summary>
        private void UpdateView()
        {
            Progress.StartAngle = 0;
            Progress.EndAngle = 0;

            // No further action needed if no event has been set.
            if (CurEvent == null)
                return;

            // Retrieve number of ticks for event start, event end and current time
            long startTicks = CurEvent.Start.Ticks;
            long endTicks = CurEvent.End.Ticks;
            long curTicks = SystemClock.Instance.Now.Ticks;
            
            // Calculate event progress, clamping to the [0.0, 1.0] interval
            double eventProgress = (double)(curTicks - startTicks) / (double)(endTicks - startTicks);
            eventProgress = Math.Min(eventProgress, 1.0);
            eventProgress = Math.Max(eventProgress, 0.0);
            Progress.EndAngle = 360.0 * eventProgress;
        }
	}
}