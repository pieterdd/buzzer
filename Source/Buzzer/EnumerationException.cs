﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.GeneralExceptions
{
    /// <summary>
    /// Exceptions caused when enumerating over a data structure that was
    /// custom-made for Buzzer.
    /// </summary>
    class EnumerationException : Exception
    {
        public IEnumerable Enumerable {
            get;
            private set;
        }

        public IEnumerator Enumerator
        {
            get;
            private set;
        }

        /// <param name="reason">Specific info about the nature of the error.</param>
        /// <param name="enrt">The IEnumerator that produced the error.</param>
        /// <param name="enm">The IEnumerable that the IEnumerator is involved with.</param>
        public EnumerationException(String reason, IEnumerator enrt, IEnumerable enm)
            : base(reason)
        {
            Enumerator = enrt;
            Enumerable = enm;
        }
    }
}
