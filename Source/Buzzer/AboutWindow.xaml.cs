﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;

namespace Buzzer.View.Windows
{
	/// <summary>
	/// Implements window with license info.
	/// </summary>
	public partial class AboutWindow : Window
	{
		public AboutWindow()
		{
			this.InitializeComponent();
			
			// Load license
            StreamResourceInfo licenseInfo = Application.GetResourceStream(new Uri("pack://application:,,,/" +
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ";component/Resources/LICENSE.txt"));
            StreamReader licenseReader = new StreamReader(licenseInfo.Stream);
            License.Text = licenseReader.ReadToEnd();
		}

        private void CloseClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
	}
}