﻿using Buzzer.Model;
using Buzzer.Model.ICSTools;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Timers;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace Buzzer
{
    /// <summary>
    /// Contains the list of calendars that are currently in use
    /// by the app. This class is not thread safe. Public functions
    /// should only be called by the thread that created the instance.
    /// </summary>
    class CWApp
    {
        /// <summary>
        /// Helps us redirect timer events to the thread
        /// that created this instance.
        /// </summary>
        private Dispatcher TimeoutDispatcher = Dispatcher.CurrentDispatcher;

        /// <summary>
        /// Stores the list of imported calendars.
        /// </summary>
        public CalendarList Calendars
        {
            get;
            private set;
        }

        /// <summary>
        /// Triggers a cache update on all calendars every minute.
        /// </summary>
        private Timer CacheUpdateTimer = new Timer();

        #region Events
        public delegate void ListUpdateHandler(object sender, EventArgs e);
        public delegate void NewActiveEventsHandler(CWApp sender, ICSEventBundle bundle);
        public delegate void CalendarFetchException(Calendar source, WebException ex);

        /// <summary>Raised when the calendar list changes.</summary>
        public event ListUpdateHandler ListUpdated;

        /// <summary>Raised when a calendar has new active events.</summary>
        public event NewActiveEventsHandler NewActiveEvents;

        /// <summary>Raised when a calendar runs into a WebException, which indicates that the
        /// destination was unreachable.</summary>
        public event CalendarFetchException CalendarFetchExceptionRaised;
        #endregion

        /// Used for debug purposes to make sure that certain functions run on the
        /// thread they were intended to run on.
        private readonly int CreatorID;

        /// <summary>
        /// Returns the application's version number, as retrieved from assembly info.
        /// </summary>
        public static String VersionNumber
        {
            get
            {
                System.Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                String versionString = "v" + version.Major + "." + version.Minor + "." + version.Revision;
                return versionString;
            }
        }

        public CWApp()
        {
            CreatorID = Environment.CurrentManagedThreadId;

            // Attempt to import the calendar list. If anything fails,
            // start with an empty list.
            try
            {
                Calendars = ImportCalendarList();

                // Wire the events and fetch all imported calendars.
                foreach (Calendar cal in Calendars)
                {
                    WireCalendar(cal);
                    cal.Fetch();
                }
            }
            catch (Exception)
            {
                Calendars = new CalendarList();
            }

            // Configure our cache update timer
            CacheUpdateTimer.Elapsed += UpdateCalendarCaches;
            CacheUpdateTimer.AutoReset = false;
            ScheduleCacheUpdate();
        }

        /// <summary>
        /// Schedules a cache update when the next minute starts.
        /// </summary>
        private void ScheduleCacheUpdate()
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

            int startin = 60 - DateTime.Now.Second;
            CacheUpdateTimer.Interval = startin * 1000;
            CacheUpdateTimer.Start();
        }

        /// <summary>
        /// Updates the cache of every calendar and schedules a new update.
        /// </summary>
        private void UpdateCalendarCaches(object sender, ElapsedEventArgs e)
        {
            TimeoutDispatcher.Invoke(delegate()
            {
                Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

                foreach (Calendar cal in Calendars)
                    cal.UpdateCache();

                ScheduleCacheUpdate();
            });
        }

        /// <summary>
        /// Adds a new calendar to the list by URL.
        /// </summary>
        public void AddCalendar(String calUrl)
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

            try
            {
                // Construct the calendar and its associated timer
                Calendar newCal = new Calendar(calUrl);
                Calendars.Add(newCal);
                WireCalendar(newCal);

                // Start the calendar download asynchronously
                newCal.Fetch();

                // Save the calendar list to disk
                ExportCalendarList();
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong while adding the calendar.");
            }
        }

        /// <summary>
        /// Wires the events of a newly added calendar and pushes a new
        /// timer for it to the timer list.
        /// </summary>
        private void WireCalendar(Calendar newCal)
        {
            // Calendar events
            newCal.Fetched += CalendarFetched;
            newCal.FetchFailed += ProcessCalendarFetchFail;
            newCal.CacheUpdated += ProcessCalendarCacheUpdate;
        }

        /// <summary>
        /// Reponds to failed a failed calendar download.
        /// </summary>
        private void ProcessCalendarFetchFail(Calendar sender, ErrorEventArgs args)
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
            Type exceptionType = args.GetException().GetType();

            // Invalid URL?
            if (exceptionType.Equals(typeof(WebException)))
            {
                if (CalendarFetchExceptionRaised != null)
                    CalendarFetchExceptionRaised(sender, (WebException)args.GetException());
            }
            // Download, parse or other error?
            else {
                String error = "";
                if (exceptionType.Equals(typeof(ArgumentException)))
                    error += "'" + sender.DataUrl + "' doesn't appear to be a valid URL. We cannot download this calendar.";
                else 
                    error += "We don't recognize '" + sender.DataUrl + "' as a calendar and we can't import it.";
                error += " Would you like us to remove it from your calendar list?";

                MessageBoxResult result = MessageBox.Show(error,
                    (String)Application.Current.Resources["AppTitle"],
                    MessageBoxButton.YesNo, MessageBoxImage.Exclamation, MessageBoxResult.No);
                if (result == MessageBoxResult.Yes)
                {
                    int calID = Calendars.IndexOf(sender);
                    Debug.Assert(calID >= 0);
                    RemoveCalendar(calID);
                }
            }
        }

        /// <summary>
        /// Deletes a calendar from storage by list position and exports
        /// the new state of the calendar list.
        /// </summary>
        public void RemoveCalendar(int id)
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
            Calendars.RemoveAt(id);
            ExportCalendarList();
        }

        /// <summary>
        /// Callback that is triggered when a calendar's data
        /// has been downloaded.
        /// </summary>
        private void CalendarFetched(object sender, EventArgs e)
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

            // Send out a notification to the GUI so that it may redraw the calendar list.
            if (ListUpdated != null)
                ListUpdated(this, EventArgs.Empty);

            // Schedule a calendar cache update
            ((Calendar)sender).UpdateCache();
        }

        /// <summary>
        /// Called when a calendar's cache has been successfully updated
        /// and holds new data.
        /// </summary>
        private void ProcessCalendarCacheUpdate(object sender, EventArgs e)
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);
            Calendar calSrc = (Calendar)sender;

            // Get the list of ongoing events and notify the UI, who
            // is hopefully listening to the event that we'll raise.
            ICSEventBundle newlyOngoing = calSrc.NewlyActiveEvents;
            if (!newlyOngoing.IsEmpty && NewActiveEvents != null)
                NewActiveEvents(this, newlyOngoing);
        }

        /// <summary>
        /// Attempts to import the saved calendar list from calendars.xml.
        /// Will throw an exception if anything fails.
        /// </summary>
        /// <returns>The imported list.</returns>
        public CalendarList ImportCalendarList()
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

            XmlSerializer serializer = new XmlSerializer(typeof(CalendarList));
            StreamReader fileReader = new StreamReader(@"calendars.xml");
            CalendarList result = (CalendarList)serializer.Deserialize(fileReader);
            fileReader.Close();
            return result;
        }

        /// <summary>
        /// Writes the list of calendar objects to disk in serialized form.
        /// </summary>
        public void ExportCalendarList()
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

            XmlSerializer serializer = new XmlSerializer(typeof(CalendarList));
            StreamWriter fileWriter = new StreamWriter(@"calendars.xml");
            serializer.Serialize(fileWriter, Calendars);
            fileWriter.Close();
        }

        /// <summary>
        /// Triggers a calendar redownload on all idle calendars.
        /// </summary>
        public void ReloadAllCalendars()
        {
            Debug.Assert(CreatorID == Environment.CurrentManagedThreadId);

            foreach (Calendar cal in Calendars)
                cal.Fetch();
        }
    }
}
