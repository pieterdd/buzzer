﻿using Buzzer.GeneralExceptions;
using Buzzer.Structures;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Structures
{
    /// <summary>
    /// Enumerator for SortedMultiList.
    /// </summary>
    public class SortedMultiListEnumerator<T, U> : IEnumerator
    {
        private SortedMultiList<T, U> Subject;
        private int KeyPosition = -1;
        private int ValuePosition = -1;

        /// <summary>Generic version of the current list item.</summary>
        object IEnumerator.Current
        {
            get
            {
                return (object) Current;
            }
        }

        /// <summary>Typed version of the current list item.</summary>
        public KeyValuePair<T, U> Current
        {
            get {
                if (KeyPosition >= Subject.Keys.Count || ValuePosition >= Subject[Subject.Keys.ElementAt(KeyPosition)].Count)
                    throw new EnumerationException("Not a valid list item", this, Subject);

                T key = Subject.Keys.ElementAt(KeyPosition);
                U value = Subject[Subject.Keys.ElementAt(KeyPosition)][ValuePosition];

                return new KeyValuePair<T, U>(key, value);
            }
        }

        public SortedMultiListEnumerator(SortedMultiList<T, U> sml)
        {
            Subject = sml;
        }

        /// <summary>Advances the iterator.</summary>
        public bool MoveNext()
        {
            // Set the next key position if necessary
            ++ValuePosition;
            while (KeyPosition < Subject.Keys.Count && (KeyPosition == -1 || ValuePosition >= Subject[Subject.Keys.ElementAt(KeyPosition)].Count))
            {
                ++KeyPosition;
                ValuePosition = 0;
            }

            // End of the list?
            if (KeyPosition >= Subject.Keys.Count)
                return false;

            // If this point is reached, we've found the next item
            return true;
        }

        /// <summary>Rewinds the iterator to the beginning of the list.</summary>
        public void Reset()
        {
            KeyPosition = -1;
            ValuePosition = -1;
        }
    }
}
