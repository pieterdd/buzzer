﻿using Buzzer.Model.ICSTools.Exceptions;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Buzzer.Model.ICSTools {
    /// <summary>
    /// Builds a hierarchical summary of a raw ICS file's 
    /// structure that eases the task of parsing it.
    /// </summary>
    class ICSTreeBuilder {
        private String[] RawLines;
        public ICSElement RootElement {
            get;
            private set;
        }
        private Stack<ICSElement> ItemStack = new Stack<ICSElement>();
        private HashSet<String> KeyFilter;
        public String MD5Hash {
            get;
            private set;
        }

        /// <param name="rawData">The raw ICS file as string</param>
        /// <param name="keyFilter">(Optional) ICSElements will register properties with these key names</param>
        public ICSTreeBuilder(HashSet<String> keyFilter = null) {
            MD5Hash = "";
            if (keyFilter != null)
                KeyFilter = keyFilter;
            else
                KeyFilter = new HashSet<string>();
        }

        /// <summary>
        /// Calculates the checksum of this ICS file based on the
        /// "LAST-MODIFIED" stamps.
        /// </summary>
        public String GetICSChecksum() {
            // Collect all "LAST-MODIFIED" lines
            String strippedRaw = "";
            foreach (String line in RawLines) {
                if (line.IndexOf("LAST-MODIFIED") == 0)
                    strippedRaw += line;
            }

            MD5 md5hasher = MD5.Create();
            byte[] hash = md5hasher.ComputeHash(Encoding.UTF8.GetBytes(strippedRaw));
            UTF8Encoding utfEncoder = new UTF8Encoding();
            return utfEncoder.GetString(hash);
        }

        /// <summary>
        /// (Re-)builds the ICS tree. Will not rebuild if the MD5
        /// hash of the new data equals that of the old data.
        /// </summary>
        /// <param name="rawData">The ICS file in String form</param>
        /// <returns>true if the tree was updated.</returns>
        public bool BuildICSTree(String rawData) {
            // Don't rebuild the tree unless the ICS file changed
            RawLines = rawData.Split(new String[] { "\r\n" }, StringSplitOptions.None);
            String newChecksum = GetICSChecksum();
            if (MD5Hash.Equals(newChecksum))
                return false;
            MD5Hash = newChecksum;

            // Process the file line per line
            for (int i = 0; i < RawLines.Length; ++i) {
                KeyValuePair<String, String> lnInterp;

                // Skip lines that are continuations of previous lines, which are 
                // marked using a whitespace at the start of the line
                if (RawLines[i].StartsWith(" "))
                    continue;
                // Skip any special property lines that don't fit into our tree format,
                // such as ATTENDEE and ORGANIZER
                else if (!Regex.Match(RawLines[i], "^[^\n: ]*:[^\n]*$").Success)
                    continue;
                // Skip blank lines
                else if (RawLines[i].Equals(""))
                    continue;

                // Swallow exceptions resulting from lines that couldn't be interpreted
                // and move on to the next line
                try {
                    lnInterp = InterpretLine(i);
                } catch (ICSParsingException) {
                    continue;
                }

                // Now that we're sure that we have a valid line, process it.
                BuildICSLine(i, lnInterp);
            }

            return true;
        }

        /// <summary>
        /// Processes one iteration of the loaded ICS file.
        /// </summary>
        /// <param name="line"></param>
        private void BuildICSLine(int line, KeyValuePair<String, String> interp) {
            if (interp.Key.Equals("BEGIN")) {
                // Start of a new element? Push its name and the line number
                // onto the element stack.
                ICSElement newEl = new ICSElement(interp.Value, line);
                if (ItemStack.Count == 0)
                    RootElement = newEl;
                else
                    ItemStack.Peek().NestedElements.Add(newEl);
                ItemStack.Push(newEl);
            } else if (interp.Key.Equals("END")) {
                // End of the current element? Write the end line number
                // and pop it off the stack.
                ICSElement curEl = ItemStack.Peek();
                if (!curEl.Type.Equals(interp.Value))
                    throw new ICSParsingException("Found END tag for '" + interp.Value + "', expected '" + curEl.Type + "'");

                curEl.EndLine = line;
                ItemStack.Pop();
            } else {
                // Regular property? If the key is in the filter, add it
                // to the topmost ICSElement on the stack.
                if (ItemStack.Count > 0 && KeyFilter.Contains(interp.Key))
                    ItemStack.Peek().Properties[interp.Key] = interp.Value;
            }
        }

        /// <summary>
        /// Returns a formatted version if the current line,
        /// assuming it follows the 'KEY:VALUE' format.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private KeyValuePair<String, String> InterpretLine(int id) {
            if (!(id >= 0 && id < RawLines.Length))
                throw new ICSParsingException("Line " + id + " is out of range");

            // Fetch the line, observing ICS line wrapping rules
            String extractedLine = RawLines[id];
            for (int lnCounter = id + 1; lnCounter < RawLines.Length && RawLines[lnCounter].StartsWith(" "); ++lnCounter)
            {
                String curRawLine = RawLines[lnCounter].Substring(1);
                extractedLine += curRawLine;
            }

            if (!Regex.Match(extractedLine, "^[^\n: ]*:[^\n]*$").Success)
                throw new ICSParsingException("Line '" + extractedLine + "' doesn't use key/value formatting");

            String key = extractedLine.Substring(0, extractedLine.IndexOf(":"));
            String value = extractedLine.Substring(extractedLine.IndexOf(":") + 1);

            // In case of 'KEY;ADDENDUM:VALUE' situations, make
            // 'ADDENDUM:VALUE' the value
            if (key.IndexOf(";") != -1) {
                value = extractedLine.Substring(extractedLine.IndexOf(";") + 1);
                key = key.Substring(0, key.IndexOf(";"));
            }

            return new KeyValuePair<String, String>(key, value);
        }
    }
}
