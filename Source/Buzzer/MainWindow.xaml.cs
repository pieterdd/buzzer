﻿using Buzzer.Model;
using Buzzer.Model.ICSTools;
using Buzzer.Notifications;
using Buzzer.View.Windows;
using NodaTime;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Media;

namespace Buzzer.View
{
    /// <summary>
    /// Interaction logic for the main window.
    /// </summary>
    public partial class MainWindow : Window
    {
        private CWApp App;
        private System.Windows.Forms.NotifyIcon TrayIcon = new System.Windows.Forms.NotifyIcon();
        private System.Windows.Forms.ContextMenu TrayMenu = new System.Windows.Forms.ContextMenu();

        public MainWindow()
        {
            // Save GUI thread ID for debug purposes and export the app name
            Application.Current.Properties["LastGUIThreadID"] = Environment.CurrentManagedThreadId;

            // Initialize app and UI
            App = new CWApp();
            App.NewActiveEvents += ShowNewActiveEvents;
            App.CalendarFetchExceptionRaised += HandleWebException;
            Closing += ConfirmClose;
            StateChanged += InterceptMinimize;
            Visibility = System.Windows.Visibility.Hidden;
            InitializeComponent();
            UILblTitle.Content = "Calendar Settings";

            // Initialize tray icon
            Stream iconStream = Application.GetResourceStream(new Uri("pack://application:,,,/" +
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ";component/Resources/AlarmClockWhite.ico")).Stream;
            TrayIcon.Icon = new System.Drawing.Icon(iconStream);
            TrayIcon.Visible = true;
            TrayIcon.Text = (String)Application.Current.Resources["AppTitle"];
            TrayIcon.MouseClick += RevealMainWindow;
            BuildTrayMenu();

            // Initialize bindings
            UICalList.ItemsSource = App.Calendars;
        }

        /// <summary>
        /// Sets up the right-click tray icon menu.
        /// </summary>
        private void BuildTrayMenu()
        {
            TrayIcon.ContextMenu = TrayMenu;

            // Opens the main window
            System.Windows.Forms.MenuItem mnuOpen = new System.Windows.Forms.MenuItem("&Configure");
            mnuOpen.Click += RevealMainWindow;
            TrayMenu.MenuItems.Add(mnuOpen);

            // Exits the program
            System.Windows.Forms.MenuItem mnuExit = new System.Windows.Forms.MenuItem("&Exit");
            mnuExit.Click += (sender, e) =>
            {
                Close();
            };
            TrayMenu.MenuItems.Add(mnuExit);
        }

        /// <summary>
        /// Renders the about window at the center of the screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowAboutWindow(object sender, EventArgs e)
        {
            AboutWindow aboutWin = new AboutWindow();
            aboutWin.ShowDialog();
        }

        /// <summary>
        /// Intercepts the minimize action when the WindowState changes.
        /// </summary>
        private void InterceptMinimize(object sender, EventArgs e)
        {
            // If you're gonna minimize the window, just hide it in the tray.
            if (WindowState == System.Windows.WindowState.Minimized)
            {
                WindowState = System.Windows.WindowState.Normal;
                DissolveMainWindow();
            }
        }

        /// <summary>
        /// Intercepts the window closing action to ask for confirmation.
        /// </summary>
        private void ConfirmClose(object sender, System.ComponentModel.CancelEventArgs e)
        {
            String prompt = "If you close " + Application.Current.Resources["AppName"] + ", you will not receive alerts for upcoming and ongoing events." +
                " To close the configuration window without closing the application, please use the minimize button.\n\n Are you sure that you want to quit?";
            MessageBoxResult result = MessageBox.Show(prompt, "Exit confirmation", MessageBoxButton.YesNo,
                MessageBoxImage.Exclamation, MessageBoxResult.No);

            // Prevent dead tray icons from piling up if the user confirms exit
            if (result != MessageBoxResult.Yes)
                e.Cancel = true;
            else
                TrayIcon.Visible = false;
        }

        /// <summary>
        /// Unhides the main window.
        /// </summary>
        private void RevealMainWindow(object sender, EventArgs e)
        {
            Visibility = System.Windows.Visibility.Visible;
            ShowInTaskbar = true;
            Focus();
        }

        /// <summary>
        /// Hides the main window without destroying it.
        /// </summary>
        private void DissolveMainWindow()
        {
            Visibility = System.Windows.Visibility.Hidden;
            ShowInTaskbar = false;
        }

        /// <summary>
        /// Displays a bundle of newly active events that just came in.
        /// </summary>
        private void ShowNewActiveEvents(CWApp sender, ICSEventBundle bundle)
        {
            // Thread safety measure
            Debug.Assert((int)Application.Current.Properties["LastGUIThreadID"] == Environment.CurrentManagedThreadId);

            // Play the alert sound
            Uri soundUri = new Uri("Resources/Reminder.mp3", UriKind.Relative);
            MediaPlayer mplay = new MediaPlayer();
            mplay.Open(soundUri);
            mplay.Volume = 1;
            mplay.Play();

            // Show the popup
            DateTimeZone tzSystem = DateTimeZoneProviders.Tzdb.GetSystemDefault();
            NfyWindow nfyWin = new NfyWindow(bundle);
            PositionWindowInCorner(nfyWin);
            nfyWin.Show();
        }

        /// <summary>
        /// Modifies the position of the supplied window so that it is placed in the
        /// bottom right corner.
        /// </summary>
        /// <param name="window"></param>
        private void PositionWindowInCorner(Window window)
        {
            System.Drawing.Rectangle workingArea = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea;
            window.Left = workingArea.Width - window.Width;
            window.Top = workingArea.Height - window.Height;
        }

        /// <summary>Brings up the calendar import window.</summary>
        private void ShowAddCalendarWindow(object sender, RoutedEventArgs e)
        {
            AddCalendarWindow acw = new AddCalendarWindow();
            acw.Owner = this;
            bool? result = acw.ShowDialog();

            // Add the specified calendar if the user okays the dialog.
            if (result == true)
                App.AddCalendar(acw.UserURL);
        }

        /// <summary>
        /// Removes the selected calendar from the list after confirming the action.
        /// </summary>
        private void RemoveSelectedCalendar(object sender, RoutedEventArgs e)
        {
            Debug.Assert(UICalList.SelectedIndex != -1);
            MessageBoxResult result = MessageBox.Show("Are you sure that you want to remove '" + App.Calendars[UICalList.SelectedIndex].Name + "' from your calendars?",
                "Remove calendar", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

            if (result == MessageBoxResult.Yes)
                App.RemoveCalendar(UICalList.SelectedIndex);
        }

        /// <summary>
        /// Triggers a redownload on every calendar unless they are busy.
        /// </summary>
        private void ReloadAll(object sender, RoutedEventArgs e)
        {
            App.ReloadAllCalendars();
        }

        /// <summary>
        /// Triggers a settings export following a setting change.
        /// </summary>
        private void SaveSettings(object sender, EventArgs e)
        {
            App.ExportCalendarList();
        }

        /// <summary>
        /// Handles a WebException coming from a calendar that couldn't be
        /// downloaded because the host couldn't be reached.
        /// </summary>
        private void HandleWebException(Calendar source, WebException webEx)
        {
            Debug.Assert((int)Application.Current.Properties["LastGUIThreadID"] == Environment.CurrentManagedThreadId);

            OfflineWindow oWin = new OfflineWindow(source.Name);
            PositionWindowInCorner(oWin);
            oWin.Show();
        }
    }
}
