﻿using Buzzer.Model.ICSTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Model.ICSTools.Exceptions
{
    /// <summary>
    /// Exceptions related to the interpretation of an existing ICS event.
    /// </summary>
    class ICSInterpretationException : Exception
    {
        public ICSEvent Event {
            get;
            private set;
        }

        /// <param name="reason">Specific info about the nature of the error.</param>
        /// <param name="item">The ICSEvent that produced the error.</param>
        public ICSInterpretationException(String reason, ICSEvent ev)
            : base(reason)
        {
            Event = ev;
        }
    }
}
