﻿using Buzzer.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buzzer.Model.ICSTools.Recurrence
{
    /// <summary>
    /// Bundles interval information (e.g. BYDAY, BYHOUR) from an iCal
    /// recurrence pattern. Throught the IntervalParameters class, we
    /// cite parts of RFC2445. For more information, see:
    /// 
    /// http://www.kanzaki.com/docs/ical/recur.html (human-readable)
    /// http://www.ietf.org/rfc/rfc2445.txt (plaintext)
    /// </summary>
    public class RecurrenceParams
    {
        /// <summary>
        /// The INTERVAL rule part contains a positive integer representing how
        /// often the recurrence rule repeats. The default value is "1", meaning
        /// every second for a SECONDLY rule, or every minute for a MINUTELY
        /// rule, every hour for an HOURLY rule, every day for a DAILY rule,
        /// every week for a WEEKLY rule, every month for a MONTHLY rule and
        /// every year for a YEARLY rule.
        /// </summary>
        public int Interval
        {
            get { return INTERNAL_Interval; }
            set
            {
                if (value < 1)
                    throw new ArgumentOutOfRangeException();
                INTERNAL_Interval = value;
            }
        }
        private int INTERNAL_Interval = 1;

        /// <summary>
        /// The BYSECOND rule part specifies a COMMA character (US-ASCII decimal
        /// 44) separated list of seconds within a minute. Valid values are 0 to
        /// 59.
        /// </summary>
        public RangedList<int> BySecond
        {
            get;
            private set;
        }

        /// <summary>
        /// The BYMINUTE rule part specifies a COMMA character (US-ASCII
        /// decimal 44) separated list of minutes within an hour. Valid values
        /// are 0 to 59.
        /// </summary>
        public RangedList<int> ByMinute
        {
            get;
            private set;
        }

        /// <summary>
        /// The BYHOUR rule part specifies a COMMA character (US-
        /// ASCII decimal 44) separated list of hours of the day. Valid values
        /// are 0 to 23.
        /// </summary>
        public RangedList<int> ByHour
        {
            get;
            private set;
        }

        /// <summary>
        /// The BYDAY rule part specifies a COMMA character (US-ASCII decimal 44)
        /// separated list of days of the week; MO indicates Monday; TU indicates
        /// Tuesday; WE indicates Wednesday; TH indicates Thursday; FR indicates
        /// Friday; SA indicates Saturday; SU indicates Sunday.
        /// 
        /// Each BYDAY value can also be preceded by a positive (+n) or negative
        /// (-n) integer. If present, this indicates the nth occurrence of the
        /// specific day within the MONTHLY or YEARLY RRULE. For example, within
        /// a MONTHLY rule, +1MO (or simply 1MO) represents the first Monday
        /// within the month, whereas -1MO represents the last Monday of the
        /// month. If an integer modifier is not present, it means all days of
        /// this type within the specified frequency. For example, within a
        /// MONTHLY rule, MO represents all Mondays within the month.
        /// </summary>
        public List<DowWithOffset> ByDay
        {
            get;
            private set;
        }        

        /// <summary>
        /// The BYWEEKNO rule part specifies a COMMA character (US-ASCII decimal
        /// 44) separated list of ordinals specifying weeks of the year. Valid
        /// values are 1 to 53 or -53 to -1. This corresponds to weeks according
        /// to week numbering as defined in [ISO 8601]. A week is defined as a
        /// seven day period, starting on the day of the week defined to be the
        /// week start (see WKST). Week number one of the calendar year is the
        /// first week which contains at least four (4) days in that calendar
        /// year. This rule part is only valid for YEARLY rules. For example, 3
        /// represents the third week of the year.
        /// </summary>
        public RangedList<int> ByWeekNo
        {
            get;
            private set;
        }

        /// <summary>
        /// The BYMONTH rule part specifies a COMMA character (US-ASCII decimal
        /// 44) separated list of months of the year. Valid values are 1 to 12.
        /// </summary>
        public RangedList<int> ByMonth
        {
            get;
            private set;
        }

        /// <summary>
        /// The BYMONTHDAY rule part specifies a COMMA character (ASCII decimal
        /// 44) separated list of days of the month. Valid values are 1 to 31 or
        /// -31 to -1. For example, -10 represents the tenth to the last day of
        /// the month.
        /// </summary>
        public RangedList<int> ByMonthDay
        {
            get;
            private set;
        }

        /// <summary>
        /// The BYYEARDAY rule part specifies a COMMA character (US-ASCII decimal
        /// 44) separated list of days of the year. Valid values are 1 to 366 or
        /// -366 to -1. For example, -1 represents the last day of the year
        /// (December 31st) and -306 represents the 306th to the last day of the
        /// year (March 1st).
        /// </summary>
        public RangedList<int> ByYearDay
        {
            get;
            private set;
        }

        /// <summary>
        /// The BYSETPOS rule part specifies a COMMA character (US-ASCII decimal
        /// 44) separated list of values which corresponds to the nth occurrence
        /// within the set of events specified by the rule. Valid values are 1 to
        /// 366 or -366 to -1. It MUST only be used in conjunction with another
        /// BYxxx rule part. For example "the last work day of the month" could
        /// be represented as:
        ///       RRULE:FREQ=MONTHLY;BYDAY=MO,TU,WE,TH,FR;BYSETPOS=-1
        ///       
        /// Each BYSETPOS value can include a positive (+n) or negative (-n)
        /// integer. If present, this indicates the nth occurrence of the
        /// specific occurrence within the set of events specified by the rule.
        /// </summary>
        public RangedList<int> BySetPos
        {
            get;
            private set;
        }

        public RecurrenceParams()
        {
            // Build exclusion list that is used for certain parameters
            List<int> zeroExcluder = new List<int>();
            zeroExcluder.Add(0);

            // Initialize time components
            BySecond = new RangedList<int>(0, 59);
            ByMinute = new RangedList<int>(0, 59);
            ByHour = new RangedList<int>(0, 23);

            // Initialize date components
            ByDay = new List<DowWithOffset>();
            ByWeekNo = new RangedList<int>(-53, 53, zeroExcluder);
            ByMonth = new RangedList<int>(1, 12);
            ByMonthDay = new RangedList<int>(-31, 31, zeroExcluder);
            ByYearDay = new RangedList<int>(-366, 366, zeroExcluder);
            BySetPos = new RangedList<int>(-366, 366, zeroExcluder);
        }
    }

    /// <summary>
    /// Data structure supporting the BYDAY attribute in recurrence patterns.
    /// </summary>
    public class DowWithOffset
    {
        public DayOfWeek Day;
        public int? Offset;

        public DowWithOffset(DayOfWeek day, int? offset = null)
        {
            Day = day;
            Offset = offset;
        }
    }
}
